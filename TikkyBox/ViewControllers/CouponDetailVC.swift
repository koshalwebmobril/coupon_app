//
//  CouponDetailVC.swift
//  TikkyBox
//
//  Created by Himanshu on 05/03/20.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class CouponDetailVC: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblCpnName: UILabel!
    @IBOutlet weak var lblCpnSku: UILabel!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblPrdtSku: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var btnRedeem: UIButton!
    
    
    
    var couponData = JSON()
    var isfromHome = false
  var isfromWishlist = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
       
    }
    

    func setupUI()
    {
        if isfromHome {
            btnRedeem.isHidden = true
        }else{
            btnRedeem.isHidden = false
        }
        containerView.dropShadow()
        btnRedeem.makeRoundCorner(8)
        
        lblCpnName.text = couponData["coupon_name"].stringValue
        if isfromWishlist{
            lblCpnSku.text = couponData["coupon_id"].stringValue
        
        }else{
        lblCpnSku.text = couponData["id"].stringValue
        }
        
        let nameArray = couponData["products"].arrayValue
        var value = [String]()
        nameArray.forEach { (data) in
            value.append(data["product_name"].stringValue)
        }
        
       lblProductName.text = value.joined(separator: ", ")
        
        lblPrdtSku.text = couponData["store_name"].stringValue
        lblEndDate.text = couponData["coupon_end_date"].stringValue
        lblDiscount.text = "$\( couponData["cut_price"].stringValue)"
        lblCategory.text = couponData["category_name"].stringValue
    }
    
    @IBAction func tapOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapOnRedeem(_ sender: Any) {
        requestRedeem()
    }
    
    
     func requestRedeem(){
                
                
                let baseUrl = "\(GlobalURL.baseURL3)\(GlobalURL.custRedeemURL)"
        
        var Id = String()
        
        if isfromWishlist{
            Id = couponData["coupon_id"].stringValue
        
        }else{
        Id = couponData["id"].stringValue
        }
        
      
                let token = UserDefaults.standard.value(forKey: "token")!
        let params:[String:Any] = ["auth":"\(token)","coupon_id":Id]
                
                MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
                
                Alamofire.request(baseUrl, method: .post,parameters: params).responseJSON { response in
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if response.result.value != nil{
                        let swiftyData = JSON(response.result.value!)
                        
                        print(swiftyData)
                        
                        if swiftyData["status"].stringValue == "true"{
                            
                           let alert = UIAlertController(title: AppName, message: "Coupon redeemed succesfully.Your Unique Coupon Code is : \(swiftyData["result"].stringValue)", preferredStyle: .alert)
                           
                           alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
                               self.navigationController?.popViewController(animated: true)
                           }))
                           
                           self.present(alert, animated: true, completion: nil)
                           
                        }else{
                            let alert = UIAlertController(title: AppName, message: "Coupon has been already redeemed.", preferredStyle: .alert)
                            
                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                            
                            self.present(alert, animated: true, completion: nil)

                        }
                    }
                }
                
            }
}


