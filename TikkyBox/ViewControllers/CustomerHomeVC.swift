//
//  CustomerHomeVC.swift
//  TikkyBox
//
//  Created by Sandeep Kumar on 06/07/2020.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit
import FSPagerView
import AnyChartiOS
import Alamofire
import SwiftyJSON
import SideMenu

class CustomerHomeVC: UIViewController,FSPagerViewDelegate,FSPagerViewDataSource {
   
    

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pagerView: FSPagerView!
    @IBOutlet weak var pageControl: FSPageControl!
    @IBOutlet weak var rightView: UIView!
    @IBOutlet weak var leftView: UIView!
    
    @IBOutlet weak var graphView: AnyChartView!
    
    lazy var uiComponent = [rightView , leftView]
    
    @IBOutlet weak var totalSavingLabel: UILabel!
    @IBOutlet weak var totalRedeemLabel: UILabel!
    var couponList = [JSON]()
    var weekData = JSON()
    var isfromHome = false
    override func viewDidLoad() {
        super.viewDidLoad()
isfromHome = true
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getHomeData()
    }
    
    func setPagerView()  {
        pagerView.dataSource = self
        pagerView.delegate = self
        pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        pagerView.interitemSpacing = 10
        pagerView.decelerationDistance = 2
        pagerView.isInfinite = true
//        pagerView.automaticSlidingInterval = 3.0
//        pagerView.transformer = FSPagerViewTransformer(type: .linear)
//        pagerView.itemSize = CGSize(width: pagerView.frame.width , height: pagerView.frame.height)
        
        pageControl.setFillColor(.gray, for: .normal)
        pageControl.setFillColor(.orange, for: .selected)
        pageControl.numberOfPages = couponList.count
        pageControl.contentHorizontalAlignment = .center
        
        for item in uiComponent{
            
            item?.layer.cornerRadius = 5
            item?.layer.shadowColor = UIColor.lightGray.cgColor
            item?.layer.shadowOpacity = 1
            item?.layer.shadowOffset = CGSize.zero
            item?.layer.shadowRadius = 2.5
            
        }
        if isfromHome{
        chart()
        }
       
    }
    
    
    @IBAction func tapOnHamburger(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let sidemenuvc = storyboard.instantiateViewController(withIdentifier: "MenuSlideVC") as! MenuSlideVC
        let menu = SideMenuNavigationController(rootViewController: sidemenuvc)
        menu.statusBarEndAlpha = 0
        menu.menuWidth = UIScreen.main.bounds.width / 1.3
        menu.presentationStyle = .menuDissolveIn
        menu.leftSide = true
        menu.presentationStyle.presentingEndAlpha = 0.65
        menu.presentationStyle.onTopShadowOpacity = 0.5
        menu.presentationStyle.onTopShadowRadius = 5
        menu.presentationStyle.onTopShadowColor = .black
        present(menu, animated: true, completion: nil)
    }
    
    func chart(){
        isfromHome = false
        let chart = AnyChart.area()
    
    chart.animation(settings: true)
    
    let crosshair = chart.crosshair()
    crosshair.enabled(enabled: true)
    crosshair.yStroke(color: "null", thickness: 0, dashpattern: "null", lineJoin: "null", lineCap: "null")
    crosshair.xStroke(color: "#fff", thickness: 1, dashpattern: "null", lineJoin: "null", lineCap: "null")
    crosshair.zIndex(zIndex: 39)
    crosshair.yLabel(index: 0).enabled(enabled: true)
    
    chart.yScale().stackMode(value: anychart.enums.ScaleStackMode.VALUE)
    
    chart.title(settings: "Last month Coupon Redeemed Frequency Report")
    
    let data: Array<DataEntry> = [
        
        CustomDataEntry(x: "Week1", value: weekData["w0"].doubleValue, value2: 17.2, value3: 16.1, value4: 5.4, value5: 5.2),
        CustomDataEntry(x: "Week2", value: weekData["w1"].doubleValue, value2: 12.204, value3: 16.823, value4: 3.457, value5: 4.210),
        CustomDataEntry(x: "Week3", value: weekData["w2"].doubleValue, value2: 10.342, value3: 13.23, value4: 2.872, value5: 2.959),
        CustomDataEntry(x: "Week4", value: weekData["w3"].doubleValue, value2: 10.577, value3: 12.518, value4: 3.929, value5: 2.704),
        
    ]
    
    let set = anychart.data.Set().instantiate()
    set.data(data: data)
    let series1Data = set.mapAs(mapping: "{x:'x', value: 'value'}")
//    chart.area(data: series1Data)
    let series1 = chart.area(data: series1Data)
    series1.name(name: "Frequencies")
//    series1.stroke(settings: "3 #fff")
//    series1.hovered().stroke(settings: "3 #fff")
//    series1.hovered().markers().enabled(enabled: true)
//    series1.hovered().markers().type(type: anychart.enums.MarkerType.CIRCLE)
//        .size(size: 4)
//        .stroke(color: "1.5 #fff")
//    series1.markers().zIndex(zIndex: 100)
    
   
        chart.legend().enabled(enabled: true)
    chart.legend().fontSize(size: 13)
    chart.legend().padding(padding: [0, 0, 20, 0])
    
    chart.xAxis(index: 0).title(settings: false)
//        chart.yAxis(index: 0).title(settings: "Total redeem coupon")
    
    chart.interactivity().hoverMode(mode: anychart.enums.HoverMode.BY_X)
    chart.tooltip().valuePrefix(value: "")
        .valuePostfix(value: " Coupons.")
        .displayMode(value: anychart.enums.TooltipDisplayMode.UNION)
        
        graphView.setChart(chart: chart)
    
    }
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return couponList.count
       }
       
       func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        
        let imagename = "\(GlobalURL.imagebaseURL)\(couponList[index]["coupon_image"].stringValue)"
        
        let urlString = imagename.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        let imgURL = URL(string: urlString )
        
        cell.imageView?.af_setImage(withURL: imgURL!, placeholderImage:  UIImage(named: "sample_icon3"))
//           cell.imageView?.image = UIImage(named: "sample_icon1")
//        cell.textLabel?.frame = CGRect(x: 10, y: 10, width: cell.frame.width-20, height: 40)
        cell.textLabel?.text = couponList[index]["coupon_name"].stringValue
        
           return cell
       }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        self.pageControl.currentPage = targetIndex
    }
    
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
         let storyboard = UIStoryboard(name: "Main", bundle: nil)
               let vc = storyboard.instantiateViewController(identifier: "CouponDetailVC") as! CouponDetailVC
               vc.couponData = couponList[index]
        vc.isfromHome = true
               self.navigationController?.pushViewController(vc, animated: true)
    }
    
    class CustomDataEntry: ValueDataEntry {
        init(x: String, value: Double, value2: Double, value3: Double, value4: Double, value5: Double) {
            super.init(x: x, value: value)
//            setValue(key: "value2", value: value2)
//            setValue(key: "value3", value: value3)
//            setValue(key: "value4", value: value4)
//            setValue(key: "value5", value: value5)
        }
    }
   
    func getHomeData(){
            
            
            let baseUrl = "\(GlobalURL.baseURL3)\(GlobalURL.custHomeURL)"
            
//            let catId = UserDefaults.standard.getCategoryId()
            let token = UserDefaults.standard.getUserId()
            let params:[String:Any] = ["auth":"\(token)"]
            
            MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
            
            Alamofire.request(baseUrl, method: .post,parameters: params).responseJSON { response in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                if response.result.value != nil{
                    let swiftyData = JSON(response.result.value!)
                    
                    print(swiftyData)
                    
                    if swiftyData["status"].stringValue == "true"{
                        
                        self.couponList = swiftyData["result"]["coupons"].arrayValue
                        self.weekData = swiftyData["result"]["weekwise_redeems"]
                        self.totalSavingLabel.text = String(format: "$%.2f", swiftyData["result"]["cut_prices"].floatValue)
                        self.totalRedeemLabel.text = String(format: "$%.2f", swiftyData["result"]["coupon_prices"].floatValue)
                        self.setPagerView()
                        
                       
                    }else{

                    }
                }
            }
            
        }
        

}
