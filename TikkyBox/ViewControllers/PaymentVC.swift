//
//  PaymentVC.swift
//  TikkyBox
//
//  Created by Himanshu on 05/03/20.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit

class PaymentVC: UIViewController {

    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var view5: UIView!
    @IBOutlet weak var view6: UIView!
    @IBOutlet weak var txtCardNo: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtExpryDate: UITextField!
    @IBOutlet weak var txtCvv: UITextField!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var btnSubmit: UIButton!
    
    lazy var uiComponent = [view1 , view2 , containerView ]
    lazy var uiOthrComponent = [view3 ,view4, view5 , view6]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        // Do any additional setup after loading the view.
    }
    
    func setupUI()
    {
        uiComponent.forEach { (setup) in
            setup?.makeRoundCorner(8)
            setup?.dropShadow()
        }
        uiOthrComponent.forEach { (setup) in
            setup?.makeRoundCorner(8)
            setup?.makeBorder(1)
        }
        btnSubmit.makeRoundCorner(8)
        bottomView.dropShadow()
    }
    
    
    
    @IBAction func tapOnPaypal(_ sender: Any) {
        
    }
    

    @IBAction func tapOnSubmit(_ sender: Any) {
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(identifier: "QrCodeVC") as! QrCodeVC
//        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func tapOnFirst(_ sender: Any) {
    }
    @IBAction func tapOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
