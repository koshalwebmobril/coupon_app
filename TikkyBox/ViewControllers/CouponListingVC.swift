//
//  CouponListingVC.swift
//  TikkyBox
//
//  Created by Himanshu on 05/03/20.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class CouponListingVC: UIViewController {
    
    
    @IBOutlet weak var tblCoupon: UITableView!
    @IBOutlet weak var blankImgView: UIImageView!
    var couponList = [JSON]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblCoupon.dataSource = self
        tblCoupon.delegate = self
        tblCoupon.separatorStyle = .none
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        getCouponList()
    }
    
    
    @IBAction func tapOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func addWishlist(_ sender: UIButton) {
        let position = sender.convert(CGPoint.zero, to: tblCoupon)
        let indexPath = tblCoupon.indexPathForRow(at: position)!
        let couponId = couponList[indexPath.row]["id"].stringValue

        likeAndDislike(id: couponId)
    }
    
    @IBAction func removeWishlist(_ sender: UIButton) {
        let position = sender.convert(CGPoint.zero, to: tblCoupon)
        let indexPath = tblCoupon.indexPathForRow(at: position)!
        let couponId = couponList[indexPath.row]["id"].stringValue
        likeAndDislike(id: couponId)
    }
    
}

extension CouponListingVC: UITableViewDataSource , UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return couponList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CouponCell", for: indexPath) as! CouponCell
        
        cell.selectionStyle = .none
        cell.lblCpnName.text = couponList[indexPath.row]["coupon_name"].stringValue
        cell.lblCpnDate.text = couponList[indexPath.row]["coupon_end_date"].stringValue
        
        let imagename = "\(GlobalURL.imagebaseURL)\(couponList[indexPath.row]["coupon_image"].stringValue)"
        
        let urlString = imagename.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        let imgURL = URL(string: urlString )
        
        cell.imgCpn?.af_setImage(withURL: imgURL!, placeholderImage:  UIImage(named: "sample_icon3"))
        
        if couponList[indexPath.row]["is_liked"].stringValue == "false"{
            
            cell.addBtn.isHidden = false
            cell.removeBtn.isHidden = true
        }else{
           cell.addBtn.isHidden = true
           cell.removeBtn.isHidden = false
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "CouponDetailVC") as! CouponDetailVC
        vc.couponData = couponList[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    
    func getCouponList(){
        
        
        let baseUrl = "\(GlobalURL.baseURL3)\(GlobalURL.couponListURL)"
        
        let userId = UserDefaults.standard.getUserId()
        let catId = UserDefaults.standard.getCategoryId()
        let params = ["auth":"\(userId)","category_id":"\(catId)"]
        
        
        Alamofire.request(baseUrl, method: .post,parameters: params).responseJSON { response in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            if response.result.value != nil{
                let swiftyData = JSON(response.result.value!)
                
                print(swiftyData)
                
                if swiftyData["status"].stringValue == "true"{
                    
                   
                    self.couponList = swiftyData["result"].arrayValue
                    
                    self.tblCoupon.reloadData()
                    
                    if self.couponList.count > 0{
                        self.tblCoupon.isHidden = false
                        self.blankImgView.isHidden = true
                    }else{
                        self.tblCoupon.isHidden = true
                        self.blankImgView.isHidden = false
                    }
                    
                }else{
                    self.tblCoupon.isHidden = true
                    self.blankImgView.isHidden = false
                    
                }
            }
        }
        
    }
    
    
    func likeAndDislike(id:String){
        /*auth
        coupon_id*/
        
        let baseUrl = "\(GlobalURL.baseURL2)\(GlobalURL.likeDislikeURL)"
        
        let token = UserDefaults.standard.value(forKey: "token")!
        
        let params:[String:Any] = ["auth":"\(token)","coupon_id":"\(id)"]
        
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseUrl, method: .get,parameters: params).responseJSON { response in
            
//            MBProgressHUD.hide(for: self.view, animated: true)
            if response.result.value != nil{
                let swiftyData = JSON(response.result.value!)
                
                print(swiftyData)
                
                if swiftyData["status"].stringValue == "true"{
                    let alert = UIAlertController(title: AppName, message: swiftyData["message"].stringValue, preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (UIAlertAction) in
                        self.getCouponList()
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                    
                   
                }else{
                    
                    
                }
            }
        }
        
    }
}
