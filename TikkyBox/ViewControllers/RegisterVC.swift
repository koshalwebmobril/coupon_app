//
//  RegisterVC.swift
//  TikkyBox
//
//  Created by Himanshu on 05/03/20.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import iOSDropDown
import CountryPickerView

class RegisterVC: UIViewController,UITextFieldDelegate,CountryPickerViewDelegate, CountryPickerViewDataSource {
    
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtMobNo: UITextField!
    @IBOutlet weak var txtDropdown: DropDown!
    @IBOutlet weak var countryListView: UIView!
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var dropdwonBtn: UIButton!
    @IBOutlet weak var flagImgView: UIImageView!
    var countryView : CountryPickerView!
    
    var countryCode :String = "+1"
    
    @IBOutlet weak var txtPassword: UITextField!
    
    
    lazy var uiComponent = [viewTop,view1 , view2 , view3 , view4]
    
    var userType = String()
    
    @IBOutlet weak var btnSignUp: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        let view1 = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: txtDropdown.frame.height))
        let button1 = UIButton(frame: CGRect(x: 0, y: 5, width: 30, height: view1.frame.height - 10))
        button1.setImage(UIImage(named: "dropdown"), for: .normal)
        button1.setImage(UIImage(named: "up"), for: .selected)
        view1.addSubview(button1)
        button1.addTarget(self, action: #selector(showUserList(_:)), for: .touchUpInside)
        
        txtDropdown.rightViewMode = .always
        txtDropdown.rightView = view1
        
        txtDropdown.optionArray = ["Customer","Retailer","Manufacturer"]
        
        txtDropdown.didSelect { (selectedText, _, _) in
            self.userType = selectedText
            print(self.userType)
        }
        
        countryView = CountryPickerView()
               
               countryView.delegate = self
               countryView.dataSource = self
               countryView?.isHidden = true
               let country = countryView.selectedCountry
               
                      flagImgView.image = country.flag
                          
                          codeLabel.text = " \(country.phoneCode)"
                          countryCode = "\(country.phoneCode)"
        
    }
    
    
    @IBAction func dropDownAction(_ sender: Any) {
           txtMobNo.resignFirstResponder()
           countryView.showCountriesList(from: self)
       }
    
    func searchBarPosition(in countryPickerView: CountryPickerView) -> SearchBarPosition {

           return .navigationBar
       }
    
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        

        
        flagImgView.image = country.flag
        
        codeLabel.text = "\(country.phoneCode)"
        countryCode = "\(country.phoneCode)"
    }
    
    @objc func showUserList(_ sender:UIButton){
        txtDropdown.resignFirstResponder()
        txtDropdown.showList()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtDropdown{
            txtDropdown.showList()
            textField.resignFirstResponder()
            return false
        }
        return true
    }
    
    
    func setupUI()
    {
        
        uiComponent.forEach { (setup) in
            setup?.makeBorder(1)
            setup?.makeRoundCorner(8)
        }
        btnSignUp.makeRoundCorner(8)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    //  Validation
    
    private func validation() -> Bool
    {
        guard let drop = txtDropdown.text ,  !drop.isEmpty else {
                   createAlert(title: AppName, message: select_user)
                   return false
               }
        guard let name = txtName.text ,  !name.isEmpty else {
            createAlert(title: AppName, message: valid_Name)
            return false
        }
        guard let email = txtEmail.text ,  !email.isEmpty else {
            createAlert(title: AppName, message: valid_Email)
            return false
        }
        
        guard CommonUtilities.shared.isValidEmail(testStr: txtEmail.text!) else {
            createAlert(title: AppName, message: valid_EmailAddress)
            return false
        }
        
        guard let mobile = txtMobNo.text ,  !mobile.isEmpty else {
            createAlert(title: AppName, message: valid_Mob)
            return false
        }
        guard (txtMobNo.text?.isValidPhoneNumber())! else {
            createAlert(title: AppName, message: validate_mob)
            return false
        }
        
        guard let password = txtPassword.text ,  !password.isEmpty else {
            createAlert(title: AppName, message: valid_password)
            return false
        }
        guard let passStrong = txtPassword.text ,  passStrong.count > 6 else {
            createAlert(title: AppName, message: valid_pass_strng)
            return false
        }
        
        return true
    }
    
    
    // RegisterApi
    
    private func registerData()
    {
        
        /*name
         email
         country_code
         mobile
         password
         device_type
         device_token*/
        
        if validation(){
            
            let parameters: [String: Any] = [
                "name" : txtName.text!,
                "email" : txtEmail.text!,
                "country_code" : countryCode,
                "mobile" : txtMobNo.text!,
                "password" : txtPassword.text!,
                "device_type" : "2",
                "device_token" : "124",
                "role" : "\(userType)"
            ]
            
            let baseUrl = "\(GlobalURL.baseURL)\(GlobalURL.registerURL)"
            
            MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
            
            Alamofire.request(baseUrl, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                .responseJSON { response in
                    print(response)
                    MBProgressHUD.hide(for: self.view, animated: true)
                    if response.result.value != nil{
                        let swiftyVarJson = JSON(response.result.value!)
                        print(swiftyVarJson)
                        
                        
                        let msgApi = swiftyVarJson["message"].stringValue
                        if swiftyVarJson["status"].stringValue == "true"
                        {
//                            UserDefaults.standard.setUserType(type: swiftyVarJson["result"]["role"].stringValue)
                            let alert = UIAlertController(title: NSLocalizedString(AppName, comment: ""), message: msgApi
                                , preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: {_ in
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewController(identifier: "OtpVerifyVC") as! OtpVerifyVC
                                vc.mobileNo = self.txtMobNo.text!
                                //                                                  vc.otp = swiftyVarJson["otp"].intValue
                                vc.otpStr = swiftyVarJson["otp"].stringValue
                                self.navigationController?.pushViewController(vc, animated: true)
                                
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                            
                            
                            
                            
                        }
                        else
                        {
                            
                            let alert = UIAlertController(title: NSLocalizedString(AppName, comment: ""), message: msgApi
                                , preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            
                        }
                    }
                    
                    
            }
        }
    }
    
    
    @IBAction func tapOnSignUp(_ sender: Any) {
        registerData()
    }
    
    
    
    @IBAction func tapOnLogin(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
