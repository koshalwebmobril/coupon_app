//
//  MyCouponVC.swift
//  TikkyBox
//
//  Created by Sandeep Kumar on 14/07/2020.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class MyCouponVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var blankImgView: UIImageView!
    var productArray = [JSON]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

       setupUI()
       self.tableView.isHidden = false
       self.tableView.reloadData()
       self.tableView.showLoader()
       Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(ProductDetailVC.removeLoader), userInfo: nil, repeats: false)
        
        getProductDetail()
    }
    

    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupUI()
       {
           tableView.dataSource = self
           tableView.delegate = self
           tableView.separatorStyle = .none
       }
       
       @objc func removeLoader(){
              tableView.hideLoader()
          }
       
    
    
    
    
}

extension MyCouponVC: UITableViewDataSource , UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell", for: indexPath) as! ProductCell
        cell.selectionStyle = .none
       
        
        let imagename = "\(GlobalURL.imagebaseURL)\(productArray[indexPath.row]["coupon_image"].stringValue)"
        
        let urlString = imagename.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        let imgURL = URL(string: urlString )
        
        cell.imgBanner?.af_setImage(withURL: imgURL!, placeholderImage:  UIImage(named: "sample_icon3"))
        
        let nameArray = productArray[indexPath.row]["products"].arrayValue
        var value = [String]()
        nameArray.forEach { (data) in
            value.append(data["product_name"].stringValue)
        }
        
        cell.lblProductName.text = value.joined(separator: ", ")
        cell.lblCpnName.text = productArray[indexPath.row]["coupon_name"].stringValue
        cell.lblDescription.text = productArray[indexPath.row]["coupon_descriptions"].stringValue
        cell.lblPrice.text = "$\(productArray[indexPath.row]["coupon_price"].stringValue)"
        cell.lblCpnExpry.text = productArray[indexPath.row]["expires_at"].stringValue
        cell.lblStore.text = productArray[indexPath.row]["store_name"].stringValue
        cell.lblCategory.text = productArray[indexPath.row]["category_name"].stringValue
        cell.uniqueCode.text = productArray[indexPath.row]["coupon_code"].stringValue
    
        return cell
    }
    
 
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    
    
    func getProductDetail(){


        let baseUrl = "\(GlobalURL.baseURL3)\(GlobalURL.custRedeemedURL)"
        
        let userId = UserDefaults.standard.getUserId()

        let params:[String:Any] = ["auth":"\(userId)"]

        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)

        Alamofire.request(baseUrl, method: .post,parameters: params).responseJSON { response in

            MBProgressHUD.hide(for: self.view, animated: true)
            if response.result.value != nil{
                let swiftyData = JSON(response.result.value!)

                print(swiftyData)

                if swiftyData["status"].stringValue == "true"{

                    self.productArray = swiftyData["result"].arrayValue
                    if self.productArray.count > 0{
                        self.tableView.isHidden = false
                        self.blankImgView.isHidden = true
                        DispatchQueue.main.async {
                             self.tableView.reloadData()
                        }
                    }
                 else{
                    self.tableView.isHidden = true
                    self.blankImgView.isHidden = false
                }


                }else{
                self.tableView.isHidden = true
                self.blankImgView.isHidden = false

                }
            }
        }

    }
    
    
    
    
}

