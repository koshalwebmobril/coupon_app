//
//  CheckoutVC.swift
//  TikkyBox
//
//  Created by Himanshu on 05/03/20.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit

class CheckoutVC: UIViewController {

    
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var btnPayNow: UIButton!
    @IBOutlet weak var tblCheckout: UITableView!
        {
        didSet
        {
            tblCheckout.dataSource = self
            tblCheckout.delegate = self
            tblCheckout.separatorStyle = .none
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

         setupUI()
        // Do any additional setup after loading the view.
    }
    
    func setupUI()
    {
        
        bottomView.dropShadow()
        btnPayNow.makeRoundCorner(8)
        btnPayNow.dropShadow()
    }

    @IBAction func tapOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func tapOnPayNow(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "PaymentVC") as! PaymentVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension CheckoutVC: UITableViewDataSource , UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 2
        {
            let detailCell = tableView.dequeueReusableCell(withIdentifier: "DeliveryDetailCell", for: indexPath) as! DeliveryDetailCell
            detailCell.selectionStyle = .none
            return detailCell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CheckoutCell", for: indexPath) as! CheckoutCell
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 2
        {
            return UITableView.automaticDimension
        }
        return 140
    }
    
    
}
