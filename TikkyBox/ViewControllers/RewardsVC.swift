//
//  RewardsVC.swift
//  TikkyBox
//
//  Created by Himanshu on 16/03/20.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireImage


class RewardsVC: UIViewController {
    
    @IBOutlet weak var collCenterData: UICollectionView!
    var rewardData = [JSON]()
    
    @IBOutlet weak var emptyImgView: UIImageView!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collCenterData.dataSource = self
        collCenterData.delegate = self
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        self.rewardList()
    }
    
    @IBAction func tapOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func claimCoupon(_ sender: UIButton) {
        
        let position = sender.convert(CGPoint.zero, to: collCenterData)
        let indexPath = collCenterData.indexPathForItem(at: position)!
        
        let productId = rewardData[indexPath.row]["id"].stringValue
        
        self.cliamReward(id: productId)
    }
    
}

extension RewardsVC: UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return rewardData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RewardCollCell", for: indexPath) as! RewardCollCell
        
        let imagename = "\(GlobalURL.imagebaseURL)\(rewardData[indexPath.row]["image"].stringValue)"
        
        let urlString = imagename.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        let imgURL = URL(string: urlString )
        
        cell.imgPromo?.af_setImage(withURL: imgURL!, placeholderImage:  UIImage(named: "sample_icon3"))
        
//        cell.lblType.text = rewardData[indexPath.row]["name"].stringValue
        
        cell.lblCpnWon.text = rewardData[indexPath.row]["reward_title"].stringValue
        
        cell.lblDiscount.text =  String(format: "Flat $%.2f Off", rewardData[indexPath.row]["reward_price"].floatValue)
       cell.claimBtn.isEnabled = true
        
        if rewardData[indexPath.row]["is_expired"].intValue == 0{
        
        cell.expDate.text = String(format: "Expires on: %@",formattedDateFromString(dateString:  rewardData[indexPath.row]["expire_date"].stringValue, withFormat: "dd MMM") ?? "")
            
            cell.contentView.alpha = 1.0
            
        }else{
            cell.expDate.text = "Expired"
            cell.claimBtn.isEnabled = false
             cell.contentView.alpha = 0.40
            
        }
        
        if rewardData[indexPath.row]["is_claimed"].intValue == 0{
            cell.claimBtn.setTitle("Claim", for: .normal)
            
            
        }else{
            cell.claimBtn.setTitle("Claimed", for: .normal)
            cell.claimBtn.isEnabled = false
            cell.contentView.alpha = 0.40
        }
        
        cell.claimBtn.layer.masksToBounds = true
        cell.claimBtn.layer.cornerRadius = cell.claimBtn.frame.height/2
        
        return cell
        
    }
    
    func formattedDateFromString(dateString: String, withFormat format: String) -> String? {
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy-MM-dd"
        if let date = inputFormatter.date(from: dateString) {
            let outputFormatter = DateFormatter()
            outputFormatter.dateFormat = format
            return outputFormatter.string(from: date)
        }
        return nil
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(identifier: "PromotionVC") as! PromotionVC
//        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cvw = collectionView.frame.size.width
        let cvh = collectionView.frame.size.height
        return CGSize(width: cvw/2.08545, height: 205)
        
        
    }
    
    func estimatedFrame(text: String, font: UIFont) -> CGRect {
        let size = CGSize(width: 200, height: 1000) // temporary size
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string: text).boundingRect(with: size,
                                                   options: options,
                                                   attributes: [NSAttributedString.Key.font: font],
                                                   context: nil)
    }
    
    
    
    func rewardList(){
        /*auth
        */
        
        let baseUrl = "\(GlobalURL.baseURL2)\(GlobalURL.rewardsURL)"
        
        let token = UserDefaults.standard.value(forKey: "token")!
        
        let params:[String:Any] = ["auth":"\(token)"]
        
        
        
        Alamofire.request(baseUrl, method: .get,parameters: params).responseJSON { response in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            if response.result.value != nil{
                let swiftyData = JSON(response.result.value!)
                
                print(swiftyData)
                
                if swiftyData["result"]["status"].stringValue == "true"{
                    self.rewardData = swiftyData["result"]["rewards"].arrayValue
                    self.collCenterData.isHidden = false
                    self.emptyImgView.isHidden = true
                    
                    self.collCenterData.reloadData()
                   
                }else{
//                    self.createAlert(title: AppName, message: swiftyData["message"].stringValue)
                    self.collCenterData.isHidden = true
                    self.emptyImgView.isHidden = false
                    
                }
            }
        }
        
    }
    
    func cliamReward(id:String){
            /*auth
            */
            
            let baseUrl = "\(GlobalURL.baseURL2)\(GlobalURL.claimRewardsURL)"
            
            let token = UserDefaults.standard.value(forKey: "token")!
            
        let params:[String:Any] = ["auth":"\(token)","reward_id":"\(id)"]
            
            MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
            
            Alamofire.request(baseUrl, method: .post,parameters: params).responseJSON { response in
                
//                MBProgressHUD.hide(for: self.view, animated: true)
                if response.result.value != nil{
                    let swiftyData = JSON(response.result.value!)
                    
                    print(swiftyData)
                    
                    if swiftyData["status"].stringValue == "true"{
                        
                        let alert = UIAlertController(title: AppName, message: "Reward claimed successfully.", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {_ in
                            self.rewardList()
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                       
                    }else{
    MBProgressHUD.hide(for: self.view, animated: true)
                        let alert = UIAlertController(title: AppName, message: "Reward already claimed.", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
            
        }

    
}
