//
//  ReportVC.swift
//  TikkyBox
//
//  Created by Sandeep Kumar on 18/05/2020.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ReportVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    
    
    
    @IBOutlet weak var blankIngView: UIImageView!
    @IBOutlet weak var txtTo: UITextField!
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var txtFrom: UITextField!
    
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var toView: UIView!
    
    @IBOutlet weak var fromView: UIView!
    
var isfrom = true
    
    var couponList = [JSON]()
     var datePicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        toView.dropShadow()
        fromView.dropShadow()
        bottomView.layer.cornerRadius = 5.0
        bottomView.dropShadow()
        showDatePicker()
        
    }
    
    
    func showDatePicker(){
       
        datePicker.maximumDate = NSDate() as Date
        datePicker.datePickerMode = .date
        
        
        
       
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        txtTo.inputAccessoryView = toolbar
        txtTo.inputView = datePicker
        
        txtFrom.inputAccessoryView = toolbar
        txtFrom.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd"
        
        if isfrom{
            
            txtFrom.text = formatter.string(from: datePicker.date)
            
        }else{
        txtTo.text = formatter.string(from: datePicker.date)
            getRedeemCoupon()
        }
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtTo{
            if !(txtFrom.text?.isValidTextField())! {
                self.createAlert(title: AppName, message: "Please enter first from date.")
                return false
            }
            isfrom = false
            return true
        }else if textField == txtFrom{
            isfrom = true
            return true
        }
        return true
    }
    

    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return couponList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReportCell", for: indexPath) as! ReportCell
        
        let imagename = "\(GlobalURL.imagebaseURL)\(couponList[indexPath.row]["coupon_image"].stringValue)"
        
        let urlString = imagename.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        let imgURL = URL(string: urlString )
        
        cell.imgView?.af_setImage(withURL: imgURL!, placeholderImage:  UIImage(named: "sample_icon3"))
        
        cell.nameLabel.text = couponList[indexPath.row]["coupon_name"].stringValue
        cell.dateLabel.text = couponList[indexPath.row]["coupon_end_date"].stringValue
        cell.savingPrice.text = "$\(couponList[indexPath.row]["cut_price"].stringValue)"
        cell.productPrice.text = "$\(couponList[indexPath.row]["coupon_price"].stringValue)"
        
        
        return cell
    }
    
    
    func getRedeemCoupon(){
        
        
        let baseUrl = "\(GlobalURL.baseURL3)\(GlobalURL.custRedeemedURL)"
        
        let userId = UserDefaults.standard.getUserId()
        let params = ["auth":"\(userId)","from":txtFrom.text ?? "","to":txtTo.text ?? ""]
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseUrl, method: .post,parameters: params).responseJSON { response in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            if response.result.value != nil{
                let swiftyData = JSON(response.result.value!)
                
                print(swiftyData)
                
                if swiftyData["status"].stringValue == "true"{
                    
                   
                    let dataArray = swiftyData["result"].arrayValue
                    self.couponList.removeAll()
                    
                    var total = Double()
                    
                    dataArray.forEach { (data) in
                        
//                        if data["is_redeemed"].intValue == 1{
                            self.couponList.append(data)
                        total = total + data["cut_price"].doubleValue
//                        }
                    }
                    
                    self.totalLabel.text = String(format: "$%.2f", total)
                   self.tableView.reloadData()
                    if self.couponList.count > 0{
                         self.tableView.isHidden = false
                         self.blankIngView.isHidden = true
                        self.bottomView.isHidden = false
                    }else{
                        self.tableView.isHidden = true
                        self.blankIngView.isHidden = false
                        self.bottomView.isHidden = true
                        
                    }
                    
                }else{
                    
                    
                }
            }
        }
        
    }
    
    
}
