//
//  ViewMoreVC.swift
//  TikkyBox
//
//  Created by Sandeep Kumar on 11/06/2020.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit
import AlamofireImage
import SwiftyJSON
import Alamofire

class ViewMoreVC: UIViewController {

    @IBOutlet weak var tblProduct: UITableView!
    
    @IBOutlet weak var titleLabel: UILabel!
    var categoryId = String()
        var categoryType = String()
        var couponArray = [JSON]()
    var titleStr = String()
        
            
        override func viewDidLoad() {
            super.viewDidLoad()

            tblProduct.dataSource = self
            tblProduct.delegate = self
            tblProduct.separatorStyle = .none
            
           
                titleLabel.text = titleStr
                
           
            
            MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
            getCouponList()
            
        }
        

        @IBAction func tapOnBack(_ sender: Any) {
            self.navigationController?.popViewController(animated: true)
        }
    
    @IBAction func viewBtnAction(_ sender: UIButton) {
        
        let position = sender.convert(CGPoint.zero, to: tblProduct)
        let indexPath = tblProduct.indexPathForRow(at: position)!
        
        
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailVC") as! ProductDetailVC
        
            vc.productId = couponArray[indexPath.row]["id"].stringValue
            
        
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func likeAction(_ sender: UIButton) {
        let position = sender.convert(CGPoint.zero, to: tblProduct)
        let indexPath = tblProduct.indexPathForRow(at: position)!
        
        
        likeAndDislike(id:  couponArray[indexPath.row]["id"].stringValue)
    }
        
    }

    extension ViewMoreVC: UITableViewDataSource , UITableViewDelegate
    {
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return couponArray.count
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
               
               return 150
           }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CouponTikkyCell", for: indexPath) as! CouponTikkyCell
            cell.selectionStyle = .none
            
            cell.lblCpnTitle.text = couponArray[indexPath.row]["coupon_name"].stringValue
            cell.lblPrice.text = "$\(couponArray[indexPath.row]["coupon_price"].stringValue)"
            cell.lblDetail.text = couponArray[indexPath.row]["coupon_descriptions"].stringValue
            cell.lblExpireDate.text = "Expire on: \( couponArray[indexPath.row]["coupon_end_date"].stringValue)"
            cell.lblStoreName.text = couponArray[indexPath.row]["store_name"].stringValue
            let imagename = "\(GlobalURL.imagebaseURL)\(couponArray[indexPath.row]["coupon_image"].stringValue)"
            
            let urlString = imagename.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            
            let imgURL = URL(string: urlString )
            
            cell.imgProduct?.af_setImage(withURL: imgURL!, placeholderImage:  UIImage(named: "sample_icon3"))
            if couponArray[indexPath.row]["is_liked"].intValue == 0{
                cell.btnRemove.setImage(UIImage(named: "heart_icon"), for: .normal )
                
            }else{
                cell.btnRemove.setImage(UIImage(named: "heart"), for: .normal )
            }
            
            return cell
        }
        
        
        
        
        func getCouponList(){
            
            
            let baseUrl = "\(GlobalURL.baseURL2)\(GlobalURL.moreCouponURL)"
            let token = UserDefaults.standard.value(forKey: "token")!
            let params:[String:Any] = ["category_id":"\(categoryId)","type":"\(categoryType)","auth":"\(token)"]
            
            
            
            Alamofire.request(baseUrl, method: .get,parameters: params).responseJSON { response in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                if response.result.value != nil{
                    let swiftyData = JSON(response.result.value!)
                    
                    print(swiftyData)
                    
                    if swiftyData["result"]["status"].stringValue == "true"{
                        
                        self.couponArray = swiftyData["result"]["coupons"].arrayValue
                        
                        self.tblProduct.isHidden = false
                        DispatchQueue.main.async {
                            self.tblProduct.reloadData()
                        }
                       
                    }else{
                        
                        self.createAlert(title: AppName, message: swiftyData["result"]["message"].stringValue)
                    }
                }
            }
            
        }
        
        func likeAndDislike(id:String){
            /*auth
            coupon_id*/
            
            let baseUrl = "\(GlobalURL.baseURL2)\(GlobalURL.likeDislikeURL)"
            
            let token = UserDefaults.standard.value(forKey: "token")!
            
            let params:[String:Any] = ["auth":"\(token)","coupon_id":"\(id)"]
            
            MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
            
            Alamofire.request(baseUrl, method: .get,parameters: params).responseJSON { response in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                if response.result.value != nil{
                    let swiftyData = JSON(response.result.value!)
                    
                    print(swiftyData)
                    
                    if swiftyData["status"].stringValue == "true"{
                        let alert = UIAlertController(title: AppName, message: swiftyData["message"].stringValue, preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (UIAlertAction) in
                            self.getCouponList()
                        }))
                        
                        self.present(alert, animated: true, completion: nil)
                        
                       
                    }else{
                        
                        
                    }
                }
            }
            
        }
        
    }
