//
//  HomeVC.swift
//  TikkyBox
//
//  Created by Himanshu on 05/03/20.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit
//import SideMenu
import Alamofire
import SwiftyJSON
import SDWebImage
import SideMenu

class HomeVC: UIViewController {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var tblCoupon: UITableView!
    
    
    @IBOutlet weak var emptyImgView: UIImageView!
    
    var status = Int()
    var headerData = ["Coupon Category" , "Rewards" , "Win Promotion"]
    let preference = UserDefaults.standard
    
    var categoryId = String()
    var featuredArray = [JSON]()
    var latestArray = [JSON]()
    var popularArray = [JSON]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        tblCoupon.dataSource = self
        tblCoupon.delegate = self
        tblCoupon.separatorStyle = .none
        
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
         getHomeData()
        categoryId = UserDefaults.standard.getCategoryId()
    }
    
    
    func setupUI()
    {
        topView.makeRoundCorner(25)
        topView.dropShadow()
    }
    
    private func apiLogout()
    {
        
        let parameters: [String: Any] = [
            "auth" : preference.string(forKey: "token")
        ]
        
        
        Alamofire.request("http://webmobril.org/dev/tikkybox/api/auth/logout", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON { response in
                print(response)
        }
    }
    
    
    
    @IBAction func tapOnHamburger(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let sidemenuvc = storyboard.instantiateViewController(withIdentifier: "MenuSlideVC") as! MenuSlideVC
        let menu = SideMenuNavigationController(rootViewController: sidemenuvc)
        menu.statusBarEndAlpha = 0
        menu.menuWidth = UIScreen.main.bounds.width / 1.3
        menu.presentationStyle = .menuDissolveIn
        menu.leftSide = true
        menu.presentationStyle.presentingEndAlpha = 0.65
        menu.presentationStyle.onTopShadowOpacity = 0.5
        menu.presentationStyle.onTopShadowRadius = 5
        menu.presentationStyle.onTopShadowColor = .black
        present(menu, animated: true, completion: nil)
    }
    
    
    @IBAction func viewMoreAction(_ sender: UIButton) {
        
        let position = sender.convert(CGPoint.zero, to: tblCoupon.tableHeaderView)
        let indexPath = tblCoupon.indexPathForRow(at: position)!
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewMoreVC") as! ViewMoreVC
        
        var type = String()
        if sender.tag == 0{
            type = "1"
            vc.titleStr = "Features Deals"
        }else if sender.tag == 1{
            type = "2"
            vc.titleStr = "Latest Deals"
            
        }else{
            type = "3"
            vc.titleStr = "Popular Deals"
        }
        
        
        vc.categoryId = categoryId
        vc.categoryType = type
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    @IBAction func viewBtnAction(_ sender: UIButton) {
        
        let position = sender.convert(CGPoint.zero, to: tblCoupon)
        let indexPath = tblCoupon.indexPathForRow(at: position)!
        
        
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailVC") as! ProductDetailVC
        if indexPath.section == 0{
            vc.productId = featuredArray[indexPath.row]["id"].stringValue
            
            
        }else if indexPath.section == 1{
            vc.productId = latestArray[indexPath.row]["id"].stringValue
            
            
        }else if indexPath.section == 2{
            vc.productId = popularArray[indexPath.row]["id"].stringValue
            
            
            
        }
        
       self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func likeAction(_ sender: UIButton) {
        let position = sender.convert(CGPoint.zero, to: tblCoupon)
        let indexPath = tblCoupon.indexPathForRow(at: position)!
        var couponId = String()
        if indexPath.section == 0{
            couponId = featuredArray[indexPath.row]["id"].stringValue
            
        }else if indexPath.section == 1{
            couponId = latestArray[indexPath.row]["id"].stringValue
            
        }else if indexPath.section == 2{
            couponId = popularArray[indexPath.row]["id"].stringValue
            
        }
        
        likeAndDislike(id: couponId)
    }
    
    
}

extension HomeVC: UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return featuredArray.count
        }else if section == 1{
            return latestArray.count
        }else{
            return popularArray.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let couponCell = tableView.dequeueReusableCell(withIdentifier: "CouponTikkyCell", for: indexPath) as! CouponTikkyCell
        couponCell.selectionStyle = .none
        
        if indexPath.section == 0{
            couponCell.lblCpnTitle.text = featuredArray[indexPath.row]["coupon_name"].stringValue
            couponCell.lblPrice.text = "$\(featuredArray[indexPath.row]["coupon_price"].stringValue)"
            couponCell.lblDetail.text = featuredArray[indexPath.row]["coupon_descriptions"].stringValue
            couponCell.lblExpireDate.text = "Expire on: \( featuredArray[indexPath.row]["coupon_end_date"].stringValue)"
            couponCell.lblStoreName.text = featuredArray[indexPath.row]["store_name"].stringValue
            let imagename = "\(GlobalURL.imagebaseURL)\(featuredArray[indexPath.row]["coupon_image"].stringValue)"
            
            let urlString = imagename.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            
            let imgURL = URL(string: urlString )
            
            couponCell.imgProduct?.af_setImage(withURL: imgURL!, placeholderImage:  UIImage(named: "sample_icon3"))
            
            if featuredArray[indexPath.row]["is_liked"].intValue == 0{
                couponCell.btnRemove.setImage(UIImage(named: "heart_icon"), for: .normal )
                
            }else{
                couponCell.btnRemove.setImage(UIImage(named: "heart"), for: .normal )
            }
            
        }else if indexPath.section == 1{
            couponCell.lblCpnTitle.text = latestArray[indexPath.row]["coupon_name"].stringValue
            couponCell.lblPrice.text = "$\(latestArray[indexPath.row]["coupon_price"].stringValue)"
            couponCell.lblDetail.text = latestArray[indexPath.row]["coupon_descriptions"].stringValue
            couponCell.lblExpireDate.text = "Expire on: \(latestArray[indexPath.row]["coupon_end_date"].stringValue)"
            couponCell.lblStoreName.text = latestArray[indexPath.row]["store_name"].stringValue
            let imagename = "\(GlobalURL.imagebaseURL)\(latestArray[indexPath.row]["coupon_image"].stringValue)"
            
            let urlString = imagename.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            
            let imgURL = URL(string: urlString )
            
            couponCell.imgProduct?.af_setImage(withURL: imgURL!, placeholderImage:  UIImage(named: "sample_icon3"))
           
            if latestArray[indexPath.row]["is_liked"].intValue == 0{
                couponCell.btnRemove.setImage(UIImage(named: "heart_icon"), for: .normal )
                
            }else{
                couponCell.btnRemove.setImage(UIImage(named: "heart"), for: .normal )
            }
        }else{
            couponCell.lblCpnTitle.text = popularArray[indexPath.row]["coupon_name"].stringValue
            couponCell.lblPrice.text = "$\(popularArray[indexPath.row]["coupon_price"].stringValue)"
            couponCell.lblDetail.text = popularArray[indexPath.row]["coupon_descriptions"].stringValue
            couponCell.lblExpireDate.text = "Expire on: \(popularArray[indexPath.row]["coupon_end_date"].stringValue)"
            couponCell.lblStoreName.text = popularArray[indexPath.row]["store_name"].stringValue
            let imagename = "\(GlobalURL.imagebaseURL)\(popularArray[indexPath.row]["coupon_image"].stringValue)"
            
            let urlString = imagename.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            
            let imgURL = URL(string: urlString )
            
            couponCell.imgProduct?.af_setImage(withURL: imgURL!, placeholderImage:  UIImage(named: "sample_icon3"))
            
            if popularArray[indexPath.row]["is_liked"].intValue == 0{
                couponCell.btnRemove.setImage(UIImage(named: "heart_icon"), for: .normal )
                
            }else{
                couponCell.btnRemove.setImage(UIImage(named: "heart"), for: .normal )
            }
        }
        
        return couponCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 150
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "HomeHeaderCell") as! HomeHeaderCell
        
        if section == 0{
            headerCell.headerLabel.text = "Features Deals"
            headerCell.viewMoreBtn.tag = 0
        }else if section == 1{
            headerCell.headerLabel.text = "Latest Deals"
            headerCell.viewMoreBtn.tag = 1
        }else {
            headerCell.headerLabel.text = "Popular Deals"
            headerCell.viewMoreBtn.tag = 2
        }
        
        
        return headerCell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    
    
    
    
    func getHomeData(){
        
        
        let baseUrl = "\(GlobalURL.baseURL2)\(GlobalURL.homeURL)"
        
        let catId = UserDefaults.standard.getCategoryId()
        let token = UserDefaults.standard.value(forKey: "token")!
        let params:[String:Any] = ["auth":"\(token)","category_id":"\(catId)"]
        
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseUrl, method: .get,parameters: params).responseJSON { response in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            if response.result.value != nil{
                let swiftyData = JSON(response.result.value!)
                
                print(swiftyData)
                
                if swiftyData["result"]["status"].stringValue == "true"{
                    
                    self.featuredArray = swiftyData["result"]["featured"].arrayValue
                    self.latestArray = swiftyData["result"]["latest"].arrayValue
                    self.popularArray = swiftyData["result"]["popular"].arrayValue
                    self.tblCoupon.isHidden = false
                    self.emptyImgView.isHidden = true
                    
                    DispatchQueue.main.async {
                        self.tblCoupon.reloadData()
                    }
                   
                }else{
//                    self.createAlert(title: AppName, message: "Coupon not found.")
                    self.tblCoupon.isHidden = true
                    self.emptyImgView.isHidden = false
                }
            }
        }
        
    }
    
    
    func likeAndDislike(id:String){
        /*auth
        coupon_id*/
        
        let baseUrl = "\(GlobalURL.baseURL2)\(GlobalURL.likeDislikeURL)"
        
        let token = UserDefaults.standard.value(forKey: "token")!
        
        let params:[String:Any] = ["auth":"\(token)","coupon_id":"\(id)"]
        
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseUrl, method: .get,parameters: params).responseJSON { response in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            if response.result.value != nil{
                let swiftyData = JSON(response.result.value!)
                
                print(swiftyData)
                
                if swiftyData["status"].stringValue == "true"{
                    let alert = UIAlertController(title: AppName, message: swiftyData["message"].stringValue, preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (UIAlertAction) in
                        self.getHomeData()
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                    
                   
                }else{
                    
                    
                }
            }
        }
        
    }
    
}



extension HomeVC: SideMenuNavigationControllerDelegate {
    
    func sideMenuWillAppear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Appearing! (animated: \(animated))")
    }
    
    func sideMenuDidAppear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Appeared! (animated: \(animated))")
    }
    
    func sideMenuWillDisappear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Disappearing! (animated: \(animated))")
    }
    
    func sideMenuDidDisappear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Disappeared! (animated: \(animated))")
    }
    
}
