//
//  AddCouponVC.swift
//  TikkyBox
//
//  Created by Sandeep Kumar on 25/06/2020.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit
import iOSDropDown
import Alamofire
import SwiftyJSON
import RSSelectionMenu


class AddCouponVC: UIViewController,UITextFieldDelegate,AddProductDelegate ,UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func addProduct(data: [String],ids:[String],indexs:[IndexPath]) {
        selectedProducts = indexs
        let str = data.joined(separator: ",")
        txtProduct.text = str
        productId = ids.joined(separator: ",")
    }
    
    
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet var commonView: [UIView]!
    
    @IBOutlet weak var txtName: UITextField!
    
    @IBOutlet weak var txtTotalPrice: UITextField!
    
    @IBOutlet weak var imageLabel: UILabel!
    @IBOutlet weak var txtDate: UITextField!
    @IBOutlet weak var txtCut: UITextField!
    @IBOutlet weak var txtDetail: UITextView!
    @IBOutlet weak var txtTerms: UITextView!
    @IBOutlet weak var plusBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var txtCategory: DropDown!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var txtProduct: UITextField!
    
    @IBOutlet weak var txtStore: DropDown!
    var productList = [JSON]()
    var storeId = String()
    var categoryId = String()
    var productId = String()
    var image = UIImage()
    var isUploadImage = false
    var selectedProducts = [IndexPath]()
    
    var imagePicker = UIImagePickerController()
    var datePicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getCategoryList()
        //        setUI()
        
    }
    
    func setUI(){
        imagePicker.delegate = self
        scrollView.isHidden = false
        
        imgView.layer.masksToBounds = true
        imgView.layer.cornerRadius = 5.0
        imgView.layer.borderColor = UIColor.orange.cgColor
        imgView.layer.borderWidth = 2.0
        
        saveBtn.layer.masksToBounds = true
        saveBtn.layer.cornerRadius = 5.0
        
        let view1 = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: txtProduct.frame.height))
        let button1 = UIButton(frame: CGRect(x: 0, y: 5, width: 30, height: view1.frame.height - 10))
        button1.setImage(UIImage(named: "dropdown"), for: .normal)
        //               button1.setImage(UIImage(named: "up"), for: .selected)
        view1.addSubview(button1)
                       button1.addTarget(self, action: #selector(showProductPopup(_:)), for: .touchUpInside)
        
        txtProduct.rightViewMode = .always
        txtProduct.rightView = view1
        
        for item in commonView{
            
            item.layer.cornerRadius = 10
            item.layer.shadowColor = UIColor.lightGray.cgColor
            item.layer.shadowOpacity = 1
            item.layer.shadowOffset = CGSize.zero
            item.layer.shadowRadius = 2.5
            
        }
        
        txtCategory.didSelect { (name, index, id) in
            self.categoryId = "\(id)"
        }
        
        txtStore.didSelect { (name, index, id) in
            self.storeId = "\(id)"
        }
        
        showDatePicker()
        
        
    }
    
    
    @objc func showProductPopup(_ sender:UIButton){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductPopupVC") as! ProductPopupVC
        vc.modalPresentationStyle = .overCurrentContext
        vc.productArray = self.productList
        vc.selectedIndex = selectedProducts
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtProduct{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductPopupVC") as! ProductPopupVC
            vc.modalPresentationStyle = .overCurrentContext
            vc.productArray = self.productList
            vc.selectedIndex = selectedProducts
            vc.delegate = self
            self.present(vc, animated: true, completion: nil)
            return false
        }
        return true
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addImageAction(_ sender: Any) {
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in self.openGallary()
            
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as? UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            
            self.createAlert(title: AppName, message: "You don't have camera")
            
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.modalPresentationStyle = .fullScreen
        self.present(imagePicker, animated: true, completion: nil)
        
    }
    
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        self.imgView.image = (info[UIImagePickerController.InfoKey.originalImage] as! UIImage)
        self.imageLabel.textColor = UIColor.blue
        self.imageLabel.text = "Uploaded"
        self.isUploadImage = true
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    @IBAction func saveAction(_ sender: Any) {
        if isValidation(){
            addCouponMethod()
        }
    }
    
    func getCategoryList(){
        
        
        let baseUrl = "\(GlobalURL.baseURL2)\(GlobalURL.categoryURL)"
        
        
        
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseUrl, method: .get).responseJSON { response in
            
            //                   MBProgressHUD.hide(for: self.view, animated: true)
            if response.result.value != nil{
                let swiftyData = JSON(response.result.value!)
                
                print(swiftyData)
                
                if swiftyData["result"]["status"].stringValue == "true"{
                    
                    let categoryArray = swiftyData["result"]["categories"].arrayValue
                    
                    var nameArray = [String]()
                    var idArray = [Int]()
                    
                    categoryArray.forEach { (data) in
                        nameArray.append(data["name"].stringValue)
                        idArray.append(data["id"].intValue)
                    }
                    
                    self.txtCategory.optionArray = nameArray
                    self.txtCategory.optionIds = idArray
                    self.getStoreList()
                    
                }else{
                    
                    
                }
            }
        }
        
    }
    
    
    func showDatePicker(){
        //Formate Date
        
        
        //        datePicker.minimumDate =
        datePicker.minimumDate = NSDate() as Date
        datePicker.datePickerMode = .date
        
        
        
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        txtDate.inputAccessoryView = toolbar
        txtDate.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY/MM/dd"
        txtDate.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func isValidation()->Bool{
        
        let price = Float(txtTotalPrice.text ?? "") ?? 0.0
        let cutprice = Float(txtCut.text ?? "") ?? 0.0
        
        if !(txtName.text?.isValidTextField())!{
            self.createAlert(title: AppName, message: "Enter Coupon name.")
            return false
        }else if !(txtCategory.text?.isValidTextField())!{
            self.createAlert(title: AppName, message: "Select Category.")
            return false
        }else if !(txtStore.text?.isValidTextField())!{
            self.createAlert(title: AppName, message: "Select Store.")
            return false
        }else if !(txtProduct.text?.isValidTextField())!{
            self.createAlert(title: AppName, message: "Select Products.")
            return false
        }else if !(txtDate.text?.isValidTextField())!{
            self.createAlert(title: AppName, message: "Select Expiry date.")
            return false
        }else if !(txtTotalPrice.text?.isValidTextField())!{
            self.createAlert(title: AppName, message: "Enter total price.")
            return false
        }else if !(txtCut.text?.isValidTextField())!{
            self.createAlert(title: AppName, message: "Enter cut price.")
            return false
        }else if !(txtDetail.text?.isValidTextField())!{
            self.createAlert(title: AppName, message: "Enter coupon details.")
            return false
        }else if !(txtTerms.text?.isValidTextField())!{
            self.createAlert(title: AppName, message: "Enter terms of use.")
            return false
        }else if isUploadImage == false {
            self.createAlert(title: AppName, message: "Upload Coupon Image.")
            return false
        }
        else if cutprice >= price{
            createAlert(title: AppName, message: "Cut price must be less than Total price.")
            return false
        }
        
        
        return true
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        if textField == txtCut{
            let price = Float(txtTotalPrice.text ?? "") ?? 0.0
            let cutprice = Float(txtCut.text ?? "") ?? 0.0
            
            if cutprice >= price{
                createAlert(title: AppName, message: "Cut price must be less than Total price.")
            }
        }
    }
    
    
    func getStoreList(){
        
        
        let baseUrl = "\(GlobalURL.baseURL3)\(GlobalURL.storeURL)"
        
        let userId = UserDefaults.standard.getUserId()
        let params = ["auth":"\(userId)"]
        //        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseUrl, method: .get,parameters: params).responseJSON { response in
            
            //            MBProgressHUD.hide(for: self.view, animated: true)
            if response.result.value != nil{
                let swiftyData = JSON(response.result.value!)
                
                print(swiftyData)
                
                if swiftyData["status"].stringValue == "true"{
                    
                    let storeList = swiftyData["result"].arrayValue
                    
                    var nameArray = [String]()
                    var idArray = [Int]()
                    
                    storeList.forEach { (data) in
                        nameArray.append(data["store_name"].stringValue)
                        idArray.append(data["id"].intValue)
                    }
                    
                    self.txtStore.optionArray = nameArray
                    self.txtStore.optionIds = idArray
                    self.getProductList()
                    
                }else{
                    
                    
                }
            }
        }
        
    }
    
    func getProductList(){
        
        
        let baseUrl = "\(GlobalURL.baseURL3)\(GlobalURL.productListURL)"
        
        let userId = UserDefaults.standard.getUserId()
        let params = ["auth":"\(userId)"]
        //        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseUrl, method: .get,parameters: params).responseJSON { response in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            if response.result.value != nil{
                let swiftyData = JSON(response.result.value!)
                
                print(swiftyData)
                
                if swiftyData["status"].stringValue == "true"{
                    
                    self.productList = swiftyData["result"].arrayValue
                    self.setUI()
                    
                    
                }else{
                    
                    
                }
            }
        }
        
    }
    
    func addCouponMethod(){
        
        /*auth
         coupon_name
         coupon_price
         coupon_descriptions
         coupon_end_date
         store_id
         added_date
         category_id
         coupon_image*/
        
        let baseURL = "\(GlobalURL.baseURL2)\(GlobalURL.addCouponURL)"
        
        print(baseURL)
        let userId = UserDefaults.standard.getUserId()
        
        let params = ["auth":"\(userId)","coupon_name":"\(txtName.text ?? "")","cut_price":"\(txtCut.text ?? "")","coupon_descriptions":"\(txtDetail.text ?? "")","coupon_end_date":"\(txtDate.text ?? "")","store_id":storeId,"added_date":"\(Date())","category_id":categoryId,"product_id":productId,"terms_of_use":"\(txtTerms.text ?? "")","coupon_price":txtTotalPrice.text ?? ""]
        
        let imgData = self.imgView.image?.jpegData(compressionQuality: 0.4)
        
        print("Login Parameters:\(params)")
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            //                DispatchQueue.main.async {
            multipartFormData.append(imgData!, withName: "coupon_image",fileName: "coupon_image.jpg", mimeType: "image/jpg")
            //                }
            
            for (key, value) in params {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, to:baseURL) { (result) in
            
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    
                    print(progress.fractionCompleted)
                    
                })
                upload.responseJSON { response in
                    
                    
                    MBProgressHUD.hide(for: self.view, animated: true)
                    
                    
                    if (response.result.value != nil){
                        
                        let swiftyJsonVar = JSON(response.result.value!)
                        
                        
                        
                        if swiftyJsonVar["status"] == true {
                            
                            let alert = UIAlertController(title: AppName, message: "Coupon added succesfully.", preferredStyle: .alert)
                            
                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
                                self.navigationController?.popViewController(animated: true)
                            }))
                            
                            self.present(alert, animated: true, completion: nil)
                            
                            
                            
                        } else {
                            
                            self.createAlert(title: AppName, message: swiftyJsonVar["message"].stringValue)
                            
                            
                        }
                    }
                }
                
            case .failure(let encodingError):
                print(encodingError)
                
            }
            
        }
        
        
    }
    
    
    
}
