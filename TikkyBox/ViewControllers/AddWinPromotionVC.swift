//
//  AddWinPromotionVC.swift
//  TikkyBox
//
//  Created by Sandeep Kumar on 16/07/2020.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class AddWinPromotionVC: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate ,UITextFieldDelegate{
    
    @IBOutlet var views: [UIView]!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtBusiness: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtLink: UITextField!
    @IBOutlet weak var txtMessage: UITextView!
    @IBOutlet weak var ImgView: UIImageView!
    @IBOutlet weak var plusBtn: UIButton!
    
    @IBOutlet weak var txtDate: UITextField!
    @IBOutlet weak var imgViewHeight: NSLayoutConstraint!
    @IBOutlet weak var imageLabel: UILabel!
    @IBOutlet weak var saveBtn: UIButton!
    
    var imagePicker = UIImagePickerController()
    var datePicker = UIDatePicker()
    var isUploadImage = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

       setUI()
    }
    
    func setUI(){
           
           imagePicker.delegate = self
           
           saveBtn.layer.masksToBounds = true
           saveBtn.layer.cornerRadius = 5.0
           
           ImgView.layer.masksToBounds = true
           ImgView.layer.cornerRadius = 5.0
           ImgView.layer.borderColor = UIColor.orange.cgColor
           ImgView.layer.borderWidth = 2.0
           
           for item in views{
               
               item.layer.cornerRadius = 10
               item.layer.shadowColor = UIColor.lightGray.cgColor
               item.layer.shadowOpacity = 1
               item.layer.shadowOffset = CGSize.zero
               item.layer.shadowRadius = 2.5
               
           }
        
        showDatePicker()
    }

    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func showDatePicker(){
           //Formate Date
           
           
           //        datePicker.minimumDate =
           datePicker.minimumDate = NSDate() as Date
           datePicker.datePickerMode = .date
           
           
           
           
           //ToolBar
           let toolbar = UIToolbar();
           toolbar.sizeToFit()
           let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
           let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
           let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
           
           toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
           
           txtDate.inputAccessoryView = toolbar
           txtDate.inputView = datePicker
           
       }
       
       @objc func donedatePicker(){
           
           let formatter = DateFormatter()
           formatter.dateFormat = "YYYY-MM-dd"
           txtDate.text = formatter.string(from: datePicker.date)
           self.view.endEditing(true)
       }
       
       @objc func cancelDatePicker(){
           self.view.endEditing(true)
       }
    
    
    
    @IBAction func addAction(_ sender: Any) {
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in self.openGallary()
            
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as? UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            
            self.createAlert(title: AppName, message: "You don't have camera")
            
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.modalPresentationStyle = .fullScreen
        self.present(imagePicker, animated: true, completion: nil)
        
    }
    
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        ImgView.image = (info[UIImagePickerController.InfoKey.originalImage] as! UIImage)
        self.imgViewHeight.constant = 150
        self.imageLabel.textColor = UIColor.blue
        self.imageLabel.text = "Uploaded"
        self.isUploadImage = true
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func saveAction(_ sender: Any) {
        if isValidation(){
            addWinPromotionMethod()
        }
    }
    
    
    
    func isValidation()->Bool{
           if !(txtName.text?.isValidTextField())!{
               self.createAlert(title: AppName, message: "Enter Win Promotion name.")
               return false
           }else if !(txtBusiness.text?.isValidTextField())!{
               self.createAlert(title: AppName, message: "Enter business name.")
               return false
           }else if !(txtEmail.text?.isValidTextField())!{
               createAlert(title: AppName, message: valid_Email)
               return false
           }else if !(txtEmail.text?.isValidEmail())!{
               self.createAlert(title: AppName, message: valid_EmailAddress)
               return false
           }else if !(txtDate.text?.isValidTextField())!{
               self.createAlert(title: AppName, message: "Enter end date.")
               return false
           }else if !(txtLink.text?.isValidTextField())!{
               self.createAlert(title: AppName, message: "Enter web link.")
               return false
           }else if !(txtLink.text?.isValidURL())!{
               self.createAlert(title: AppName, message: "Enter valid web link(must start with http:// or https://).")
               return false
           }else if !(txtMessage.text?.isValidTextField())!{
               self.createAlert(title: AppName, message: "Enter Win Promotion details.")
               return false
           }else if isUploadImage == false {
               self.createAlert(title: AppName, message: "Upload banner Image.")
               return false
           }
           
           
           return true
       }
    
    
    func addWinPromotionMethod(){
                
             
                   
                   let baseURL = "\(GlobalURL.baseURL3)\(GlobalURL.addWinPromotionURL)"
                   
                   print(baseURL)
                   let userId = UserDefaults.standard.getUserId()
                   
        let params = ["auth":"\(userId)","name":"\(txtName.text ?? "")","business_name":"\(txtBusiness.text ?? "")","email":"\(txtEmail.text ?? "")","weblink":"\(txtLink.text ?? "")","desc":"\(txtMessage.text ?? "")","end_date":"\(txtDate.text ?? "")"]
                   
           let imgData = self.ImgView.image?.jpegData(compressionQuality: 0.4)
                   
                   print("Login Parameters:\(params)")
                   MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
                   
                   Alamofire.upload(multipartFormData: { multipartFormData in
       //                DispatchQueue.main.async {
                           multipartFormData.append(imgData!, withName: "promo_image",fileName: "promo_image.jpg", mimeType: "image/jpg")
       //                }
                       
                       for (key, value) in params {
                           multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                       }
                   }, to:baseURL) { (result) in
                       
                       switch result {
                       case .success(let upload, _, _):
                           upload.uploadProgress(closure: { (progress) in
                               
                               print(progress.fractionCompleted)
                               
                           })
                           upload.responseJSON { response in
                               
                               
                               MBProgressHUD.hide(for: self.view, animated: true)
                               
                               
                               if (response.result.value != nil){
                                   
                                   let swiftyJsonVar = JSON(response.result.value!)
                                   
                                   print(swiftyJsonVar)
                                   
                                   if swiftyJsonVar["status"] == true {
                                       
                                        let alert = UIAlertController(title: AppName, message: "Win Promotion created succesfully.", preferredStyle: .alert)
                                              alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
                                               self.navigationController?.popViewController(animated: true)
                                              }))
                                             
                                       
                                       self.present(alert, animated: true, completion:nil)
                                       
                                   } else {
                                       
                                       self.createAlert(title: AppName, message: swiftyJsonVar["message"].stringValue)
                                       
                                       
                                   }
                               }
                           }
                           
                       case .failure(let encodingError):
                           print(encodingError)
                           
                       }
                       
                   }
                   
               
           }
       
}
