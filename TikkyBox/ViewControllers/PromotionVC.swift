//
//  PromotionVC.swift
//  TikkyBox
//
//  Created by Himanshu on 16/03/20.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import SDWebImage

class PromotionVC: UIViewController {

    @IBOutlet weak var tblPromotion: UITableView!
    
    @IBOutlet weak var promotionHeight: NSLayoutConstraint!
    @IBOutlet weak var promoBtn: UIButton!
    var promotionArray = [JSON]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tblPromotion.dataSource = self
        tblPromotion.delegate = self
//        tblPromotion.separatorStyle = .singleLine
        
        promoBtn.layer.masksToBounds = true
        promoBtn.layer.cornerRadius = 5.0
        
        if UserDefaults.standard.getUserType() == "Customer"{
            promotionHeight.constant = 0
        }else{
            promotionHeight.constant = 50
        }
        
        
    }
    
    @IBAction func tapOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
                if UserDefaults.standard.getUserType() == "Customer"{
        getAllWinPromotion()
                }else{
        getWinPromotion()
                }
        
        
    }
    
    
    @IBAction func addWinAction(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(identifier: "AddWinPromotionVC") as! AddWinPromotionVC
                
                self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension PromotionVC: UITableViewDataSource, UITableViewDelegate
{

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return promotionArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let secondCell = tableView.dequeueReusableCell(withIdentifier: "PromoCell", for: indexPath) as! PromoCell
        secondCell.selectionStyle = .none
        
        secondCell.containerView.layer.borderColor = UIColor.secondarySystemBackground.cgColor
        secondCell.containerView.layer.borderWidth = 2.0
        
        let imagename = "\(GlobalURL.imagebaseURL)\(promotionArray[indexPath.row]["image"].stringValue)"
        
        let urlString = imagename.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        let imgURL = URL(string: urlString )
        
        secondCell.imgPromo?.af_setImage(withURL: imgURL!, placeholderImage: UIImage(named: "sample_icon1"))

        return secondCell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if UserDefaults.standard.getUserType() == "Customer"{
            
            var urlString =  promotionArray[indexPath.row]["promotion_url"].stringValue
            let checkStr = urlString.lowercased()
                if checkStr.starts(with: "http://") || checkStr.starts(with: "https://"){
                    
                }else if checkStr.contains("www"){
                    urlString = "http://\(urlString)"
                }
             let urlStr = urlString.components(separatedBy: .whitespaces).joined()
            
            if let url = URL(string: urlStr) {
                       UIApplication.shared.open(url)
                   }
       
        }else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(identifier: "PromotionDetailVC") as! PromotionDetailVC
            vc.winData = promotionArray[indexPath.row]
//                    vc.productId = promotionArray[indexPath.row]["id"].stringValue
                    self.navigationController?.pushViewController(vc, animated: true)
        }
//
    }
    
    
    
    func getWinPromotion(){
        
        
        let baseUrl = "\(GlobalURL.baseURL3)\(GlobalURL.listWinPromotionURL)"
        

        let token = UserDefaults.standard.getUserId()
        
        let params = ["auth":token]
        
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseUrl, method: .get,parameters: params).responseJSON { response in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            if response.result.value != nil{
                let swiftyData = JSON(response.result.value!)
                
                print(swiftyData)
                
                if swiftyData["status"].stringValue == "true"{
                    
                    self.promotionArray = swiftyData["result"].arrayValue
                    self.tblPromotion.isHidden = false
                    DispatchQueue.main.async {
                         self.tblPromotion.reloadData()
                    }
                   
                    
                   
                }else{
                    
                    
                }
            }
        }
        
    }
    
    
    func getAllWinPromotion(){
            
            
            let baseUrl = "\(GlobalURL.baseURL3)\(GlobalURL.listWinPromotionURL)"
            
   
            MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
            
            Alamofire.request(baseUrl, method: .get).responseJSON { response in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                if response.result.value != nil{
                    let swiftyData = JSON(response.result.value!)
                    
                    print(swiftyData)
                    
                    if swiftyData["status"].stringValue == "true"{
                        
                        self.promotionArray = swiftyData["result"].arrayValue
                        self.tblPromotion.isHidden = false
                        DispatchQueue.main.async {
                             self.tblPromotion.reloadData()
                        }
                       
                        
                       
                    }else{
                        
                        
                    }
                }
            }
            
        }
    
}
