//
//  RedeemVC.swift
//  TikkyBox
//
//  Created by Sandeep Kumar on 10/07/2020.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class RedeemVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    

    @IBOutlet weak var listLabel: UILabel!
    @IBOutlet weak var tableVIEW: UITableView!
    @IBOutlet weak var txtMob: UITextField!
    @IBOutlet weak var txtCode: UITextField!
    @IBOutlet weak var redeemBtn: UIButton!
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    
    @IBOutlet weak var tableheight: NSLayoutConstraint!
    
    var productArray = [JSON]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUI()
    }
    
    func setUI(){
    
   
    
    redeemBtn.layer.masksToBounds = true
    redeemBtn.layer.cornerRadius = 5.0
    
   
    
        view1.layer.cornerRadius = 10
        view1.layer.shadowColor = UIColor.lightGray.cgColor
        view1.layer.shadowOpacity = 1
        view1.layer.shadowOffset = CGSize.zero
        view1.layer.shadowRadius = 2.5
        
        view2.layer.cornerRadius = 10
        view2.layer.shadowColor = UIColor.lightGray.cgColor
        view2.layer.shadowOpacity = 1
        view2.layer.shadowOffset = CGSize.zero
        view2.layer.shadowRadius = 2.5
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell", for: indexPath) as! ProductCell
        cell.titleLabel.text = productArray[indexPath.row]["product_name"].stringValue
        
        
        return cell
    }
    @IBAction func redeemAction(_ sender: Any) {
        if isValidation(){
            redeemCoupon()
        }
    }
    
    
    func isValidation()->Bool{
        if !(txtMob.text?.isValidTextField())!{
            self.createAlert(title: AppName, message: "Enter customer phone number.")
            return false
        }else if !(txtMob.text?.isValidPhoneNumber())!{
            createAlert(title: AppName, message: validate_mob)
            return false
        }else if !(txtCode.text?.isValidTextField())!{
            self.createAlert(title: AppName, message: "Enter coupon unique code.")
            return false
        }
        
        
        return true
    }
    

    func redeemCoupon(){
        
        
        let baseUrl = "\(GlobalURL.baseURL3)\(GlobalURL.shopRedeemURL)"
        
        let userId = UserDefaults.standard.getUserId()
        let params = ["auth":"\(userId)","mobile":txtMob.text ?? "","code":txtCode.text ?? ""]
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseUrl, method: .post,parameters: params).responseJSON { response in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            if response.result.value != nil{
                let swiftyData = JSON(response.result.value!)
                
                print(swiftyData)
                
                if swiftyData["status"].stringValue == "true"{
                    
//                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProductPopupVC") as! ProductPopupVC
//                           vc.modalPresentationStyle = .overCurrentContext
                    self.productArray = swiftyData["result"]["products"].arrayValue
//                           vc.selectedIndex = selectedProducts
//                           vc.delegate = self
//                    vc.isFromRedeem = true
                     
//                           self.present(vc, animated: true, completion: nil)
                    
                    let alert = UIAlertController(title: AppName, message: swiftyData["message"].stringValue, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
                        self.tableheight.constant = CGFloat(self.productArray.count * 60)
                        self.tableVIEW.isHidden = false
                        self.listLabel.isHidden = false
                        self.tableVIEW.reloadData() 
                    }))


                    self.present(alert, animated: true, completion:nil)
                    
                    
                    
                }else{
                    let alert = UIAlertController(title: AppName, message: "Coupon doesn't belong to your store or redeemed.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    
                    
                    self.present(alert, animated: true, completion:nil)
                    
                }
            }
        }
        
    }
    
    
}
