//
//  ProductDetailVC.swift
//  TikkyBox
//
//  Created by Himanshu on 05/03/20.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit
import ListPlaceholder
import SwiftyJSON
import Alamofire
import SDWebImage

class ProductDetailVC: UIViewController {
    
    @IBOutlet weak var tblProductDetail: UITableView!
    var productId = String()
    
    var productArray = JSON()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        self.tblProductDetail.isHidden = false
        self.tblProductDetail.reloadData()
        self.tblProductDetail.showLoader()
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(ProductDetailVC.removeLoader), userInfo: nil, repeats: false)
//        getProductDetail()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func setupUI()
    {
        tblProductDetail.dataSource = self
        tblProductDetail.delegate = self
        tblProductDetail.separatorStyle = .none
    }
    
    @objc func removeLoader(){
           tblProductDetail.hideLoader()
       }
    
    
    @IBAction func tapOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension ProductDetailVC: UITableViewDataSource , UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell", for: indexPath) as! ProductCell
        cell.selectionStyle = .none
       
        
        let imagename = "\(GlobalURL.imagebaseURL)\(productArray["coupon_image"].stringValue)"
        
        let urlString = imagename.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        let imgURL = URL(string: urlString )
        
        cell.imgBanner?.af_setImage(withURL: imgURL!, placeholderImage:  UIImage(named: "sample_icon3"))
        
        let nameArray = productArray["products"].arrayValue
        var value = [String]()
        nameArray.forEach { (data) in
            value.append(data["product_name"].stringValue)
        }
        
        cell.lblProductName.text = value.joined(separator: ", ")
        cell.lblCpnName.text = productArray["coupon_name"].stringValue
        cell.lblDescription.text = productArray["coupon_descriptions"].stringValue
        cell.lblPrice.text = "$\(productArray["coupon_price"].stringValue)"
        cell.lblCpnExpry.text = productArray["coupon_end_date"].stringValue
        cell.lblStore.text = productArray["store_name"].stringValue
        cell.lblCategory.text = productArray["category_name"].stringValue
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(identifier: "TikkyCouponVC") as! TikkyCouponVC
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    
    
//    func getProductDetail(){
//
//
//        let baseUrl = "\(GlobalURL.baseURL2)\(GlobalURL.couponDetailURL)"
//
//        let params:[String:Any] = ["coupon_id":"\(productId)"]
//
//        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
//
//        Alamofire.request(baseUrl, method: .get,parameters: params).responseJSON { response in
//
//            MBProgressHUD.hide(for: self.view, animated: true)
//            if response.result.value != nil{
//                let swiftyData = JSON(response.result.value!)
//
//                print(swiftyData)
//
//                if swiftyData["result"]["status"].stringValue == "true"{
//
//                    self.productArray = swiftyData["result"]["coupons"]
//                    self.tblProductDetail.isHidden = false
//                    DispatchQueue.main.async {
//                         self.tblProductDetail.reloadData()
//                    }
//
//
//
//                }else{
//
//
//                }
//            }
//        }
//
//    }
    
    
    
    
}
