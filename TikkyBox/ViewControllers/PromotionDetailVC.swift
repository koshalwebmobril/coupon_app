//
//  PromotionDetailVC.swift
//  TikkyBox
//
//  Created by Sandeep Kumar on 16/07/2020.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class PromotionDetailVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var winData = JSON()
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
}

extension PromotionDetailVC: UITableViewDataSource , UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell", for: indexPath) as! ProductCell
        cell.selectionStyle = .none
       
        
        let imagename = "\(GlobalURL.imagebaseURL)\(winData["image"].stringValue)"
        
        let urlString = imagename.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        let imgURL = URL(string: urlString )
        
        cell.imgBanner?.af_setImage(withURL: imgURL!, placeholderImage:  UIImage(named: "sample_icon3"))
        
       
        
        
        cell.lblName.text = winData["promotion_name"].stringValue
        cell.lblBusiness.text = "Win Promotion"
        cell.lblEmail.text = "cuopon@gmail.com"
        cell.lblLink.text = winData["promotion_url"].stringValue
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(linkTapped))
        cell.lblLink.addGestureRecognizer(tapGesture)
        cell.lblDetail.text = winData["description"].stringValue
        
        return cell
    }
    
    @objc func linkTapped(){
        var urlString =  winData["promotion_url"].stringValue
        let checkStr = urlString.lowercased()
            if checkStr.starts(with: "http://") || checkStr.starts(with: "https://"){
                
            }else if checkStr.contains("www"){
                urlString = "http://\(urlString)"
            }
        let urlStr = urlString.components(separatedBy: .whitespaces).joined()
        
        if let url = URL(string: urlStr) {
                   UIApplication.shared.open(url)
               }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
