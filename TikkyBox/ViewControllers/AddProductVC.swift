//
//  AddProductVC.swift
//  TikkyBox
//
//  Created by Sandeep Kumar on 01/07/2020.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit
import iOSDropDown
import Alamofire
import SwiftyJSON

class AddProductVC: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet var views: [UIView]!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtPrice: UITextField!
    @IBOutlet weak var txtStore: DropDown!
    
    @IBOutlet weak var imageLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var txtDesc: UITextView!
    @IBOutlet weak var ImgView: UIImageView!
    @IBOutlet weak var saveBtn: UIButton!
    var imagePicker = UIImagePickerController()
    var isUploadImage = false
    var storeNameArray = [String]()
    var storeIdArray = [Int]()
    var storeId = Int()
    var productDict = JSON()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        getStoreList()
    }
    
    func setUI(){
        
        imagePicker.delegate = self
        
        
        saveBtn.layer.masksToBounds = true
        saveBtn.layer.cornerRadius = 5.0
        
        ImgView.layer.masksToBounds = true
        ImgView.layer.cornerRadius = 5.0
        ImgView.layer.borderColor = UIColor.orange.cgColor
        ImgView.layer.borderWidth = 2.0
        
        txtDesc.layer.masksToBounds = true
        txtDesc.layer.cornerRadius = 10
        txtDesc.layer.borderColor = UIColor.lightGray.cgColor
        txtDesc.layer.borderWidth = 2.0
        
        
        for item in views{
            
            item.layer.cornerRadius = 10
            item.layer.shadowColor = UIColor.lightGray.cgColor
            item.layer.shadowOpacity = 1
            item.layer.shadowOffset = CGSize.zero
            item.layer.shadowRadius = 2.5
            
        }
        let view1 = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: txtStore.frame.height))
        let button1 = UIButton(frame: CGRect(x: 0, y: 5, width: 30, height: view1.frame.height - 10))
        button1.setImage(UIImage(named: "dropdown"), for: .normal)
        button1.setImage(UIImage(named: "up"), for: .selected)
        view1.addSubview(button1)
        button1.addTarget(self, action: #selector(showUserList(_:)), for: .touchUpInside)
        
        txtStore.rightViewMode = .always
        txtStore.rightView = view1
        
        txtStore.optionArray = storeNameArray
        txtStore.optionIds = storeIdArray
        
        txtStore.didSelect { (selectedText, index, id) in
            
            self.storeId = id
            print(selectedText,index,id)
        }
        
        
        if productDict.count > 0{
            isUploadImage = true
            
            txtName.text = productDict["product_name"].stringValue
            txtPrice.text = productDict["product_price"].stringValue
            txtDesc.text = productDict["product_descriptions"].stringValue
            self.storeId = productDict["store_id"].intValue

            let imagename = "\(GlobalURL.imagebaseURL)\(productDict["product_image"].stringValue)"
            
            let urlString = imagename.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            
            let imgURL = URL(string: urlString )
            
            ImgView?.af_setImage(withURL: imgURL!, placeholderImage:  UIImage(named: "sample_icon3"))
            
            self.titleLabel.text = "Product Details"
            saveBtn.setTitle("Update Product", for: .normal)
            
        }else{
            self.titleLabel.text = "Add Product"
        }
        
    }
    
    @objc func showUserList(_ sender:UIButton){
        txtStore.resignFirstResponder()
        txtStore.showList()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtStore{
            txtStore.showList()
            textField.resignFirstResponder()
            return false
        }
        return true
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addAction(_ sender: Any) {
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in self.openGallary()
            
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as? UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            
            self.createAlert(title: AppName, message: "You don't have camera")
            
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.modalPresentationStyle = .fullScreen
        self.present(imagePicker, animated: true, completion: nil)
        
    }
    
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        ImgView.image = (info[UIImagePickerController.InfoKey.originalImage] as! UIImage)
        self.imageLabel.text = "Uploaded"
        self.imageLabel.textColor = UIColor.blue
        self.isUploadImage = true
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    func isValidation()->Bool{
           if !(txtName.text?.isValidTextField())!{
               self.createAlert(title: AppName, message: "Enter Product name.")
               return false
           }else if !(txtPrice.text?.isValidTextField())!{
               self.createAlert(title: AppName, message: "Enter discounted price.")
               return false
           }else if !(txtStore.text?.isValidTextField())!{
               self.createAlert(title: AppName, message: "Enter Store name.")
               return false
           }else if !(txtDesc.text?.isValidTextField())!{
               self.createAlert(title: AppName, message: "Enter product descriptions.")
               return false
           }else if isUploadImage == false {
               self.createAlert(title: AppName, message: "Upload Product Image.")
               return false
           }
           
           
           return true
       }
    
    @IBAction func saveAction(_ sender: UIButton) {
        
        if isValidation(){
            if  sender.currentTitle == "Update Product"{
                updateProduct()
            }else{
            
            addProductMethod()
            }
        }
    }
    
    
    
    func addProductMethod(){
             
        /*auth
        product_name
        product_descriptions
        product_price
        product_image*/
          
                
                let baseURL = "\(GlobalURL.baseURL3)\(GlobalURL.addProductURL)"
                
                print(baseURL)
                let userId = UserDefaults.standard.getUserId()
                
        let params = ["auth":"\(userId)","product_name":"\(txtName.text ?? "")","product_descriptions":"\(txtDesc.text ?? "")","product_price":"\(txtPrice.text ?? "")","store_id":"\(storeId)"]
                
        let imgData = self.ImgView.image?.jpegData(compressionQuality: 0.4)
                
                print("Login Parameters:\(params)")
                MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
                
                Alamofire.upload(multipartFormData: { multipartFormData in
    //                DispatchQueue.main.async {
                        multipartFormData.append(imgData!, withName: "product_image",fileName: "product_image.jpg", mimeType: "image/jpg")
    //                }
                    
                    for (key, value) in params {
                        multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                    }
                }, to:baseURL) { (result) in
                    
                    switch result {
                    case .success(let upload, _, _):
                        upload.uploadProgress(closure: { (progress) in
                            
                            print(progress.fractionCompleted)
                            
                        })
                        upload.responseJSON { response in
                            
                            
                            MBProgressHUD.hide(for: self.view, animated: true)
                            
                            
                            if (response.result.value != nil){
                                
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                
                                
                                if swiftyJsonVar["status"] == true {
                                    
                                    
                                     let alert = UIAlertController(title: AppName, message: "Product added succesfully.", preferredStyle: .alert)
                                           alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
                                            print("gaya...")
                                            self.navigationController?.popViewController(animated: true)
                                           }))


                                    self.present(alert, animated: true, completion:nil)
                                    
                                } else {
                                    
                                    self.createAlert(title: AppName, message: swiftyJsonVar["message"].stringValue)
                                    
                                    
                                }
                            }
                        }
                        
                    case .failure(let encodingError):
                        print(encodingError)
                        
                    }
                    
                }
                
            
        }
    
    
    func updateProduct(){
             
        /*auth
        product_name
        product_descriptions
        product_price
        product_image*/
          
                
                let baseURL = "\(GlobalURL.baseURL3)\(GlobalURL.updateProductURL)"
                
                print(baseURL)
                let userId = UserDefaults.standard.getUserId()
                
        let params = ["auth":"\(userId)","product_name":"\(txtName.text ?? "")","product_descriptions":"\(txtDesc.text ?? "")","product_price":"\(txtPrice.text ?? "")","store_id":"\(storeId)","product_id":productDict["id"].stringValue]
                
        let imgData = self.ImgView.image?.jpegData(compressionQuality: 0.4)
                
                print("Login Parameters:\(params)")
                MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
                
                Alamofire.upload(multipartFormData: { multipartFormData in
    //                DispatchQueue.main.async {
                        multipartFormData.append(imgData!, withName: "product_image",fileName: "product_image.jpg", mimeType: "image/jpg")
    //                }
                    
                    for (key, value) in params {
                        multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                    }
                }, to:baseURL) { (result) in
                    
                    switch result {
                    case .success(let upload, _, _):
                        upload.uploadProgress(closure: { (progress) in
                            
                            print(progress.fractionCompleted)
                            
                        })
                        upload.responseJSON { response in
                            
                            
                            MBProgressHUD.hide(for: self.view, animated: true)
                            
                            
                            if (response.result.value != nil){
                                
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                
                                
                                if swiftyJsonVar["status"] == true {
                                    
                                     let alert = UIAlertController(title: AppName, message: "Product updated succesfully.", preferredStyle: .alert)
                                           alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
                                            self.navigationController?.popViewController(animated: true)
                                           }))
                                          
                                    
                                    self.present(alert, animated: true, completion:nil)
                                    
                                } else {
                                    
                                    self.createAlert(title: AppName, message: swiftyJsonVar["message"].stringValue)
                                    
                                    
                                }
                            }
                        }
                        
                    case .failure(let encodingError):
                        print(encodingError)
                        
                    }
                    
                }
                
            
        }
    
    func getStoreList(){
        
        
        let baseUrl = "\(GlobalURL.baseURL3)\(GlobalURL.storeURL)"
        
        let userId = UserDefaults.standard.getUserId()
        let params = ["auth":"\(userId)"]
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseUrl, method: .get,parameters: params).responseJSON { response in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            if response.result.value != nil{
                let swiftyData = JSON(response.result.value!)
                
                print(swiftyData)
                
                if swiftyData["status"].stringValue == "true"{
                    
                    let storeList = swiftyData["result"].arrayValue
                    
                    self.scrollView.isHidden = false
                    
                    storeList.forEach { (data) in
                        self.storeNameArray.append(data["store_name"].stringValue)
                        self.storeIdArray.append(data["id"].intValue)
                        if self.productDict["store_id"].intValue == data["id"].intValue{
                            self.txtStore.text = data["store_name"].stringValue
                        }
                    }
                    
                    self.setUI()
                    
                }else{
                    
                    
                }
            }
        }
    }
}
