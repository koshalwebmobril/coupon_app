//
//  ProductListVC.swift
//  TikkyBox
//
//  Created by Sandeep Kumar on 01/07/2020.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage

class ProductListVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addBtn: UIButton!
    var productList = [JSON]()
    @IBOutlet weak var blankImgView: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addBtn.layer.masksToBounds = true
        addBtn.layer.cornerRadius  = 5.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getProductList()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CouponCell", for: indexPath) as! CouponCell
        cell.imgCpn.layer.masksToBounds = true
        cell.imgCpn.layer.cornerRadius = 5.0
        
        let imagename = "\(GlobalURL.imagebaseURL)\(productList[indexPath.row]["product_image"].stringValue)"
        
        let urlString = imagename.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        let imgURL = URL(string: urlString )
        
        cell.imgCpn?.af_setImage(withURL: imgURL!, placeholderImage:  UIImage(named: "sample_icon3"))
        
        cell.lblCpnName.text = productList[indexPath.row]["product_name"].stringValue
        cell.lblCpnDate.text = "$\(productList[indexPath.row]["product_price"].stringValue)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddProductVC") as! AddProductVC
        vc.productDict = productList[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func deleteProductAction(_ sender: UIButton) {
        let position = sender.convert(CGPoint.zero, to: tableView)
        let indexPath = tableView.indexPathForRow(at: position)!
        let id = productList[indexPath.row]["id"].stringValue
        let alert = UIAlertController(title: AppName, message: "Do you want to delete product?.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { _ in
            self.productList.remove(at: indexPath.row)
            self.deleteProduct(id: id)
        }))
        
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion:nil)
        
        
        
        
    }
    
    
   
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func addStoreAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddProductVC") as! AddProductVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func getProductList(){
        
        
        let baseUrl = "\(GlobalURL.baseURL3)\(GlobalURL.productListURL)"
        
        let userId = UserDefaults.standard.getUserId()
        let params = ["auth":"\(userId)"]
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseUrl, method: .get,parameters: params).responseJSON { response in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            if response.result.value != nil{
                let swiftyData = JSON(response.result.value!)
                
                print(swiftyData)
                
                if swiftyData["status"].stringValue == "true"{
                    
                    self.productList = swiftyData["result"].arrayValue
                    
                    if self.productList.count > 0{
                    self.tableView.isHidden = false
                    self.blankImgView.isHidden = true
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        
                    }
                    }else{
                        self.tableView.isHidden = true
                        self.blankImgView.isHidden = false
                    }
                    
                    
                    
                }else{
                    
                    
                }
            }
        }
        
    }
    
    
    func deleteProduct(id:String){
        
        
        let baseUrl = "\(GlobalURL.baseURL3)\(GlobalURL.deleteProductURL)"
        
        let userId = UserDefaults.standard.getUserId()
        let params = ["auth":"\(userId)","product_id":"\(id)"]
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseUrl, method: .get,parameters: params).responseJSON { response in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            if response.result.value != nil{
                let swiftyData = JSON(response.result.value!)
                
                print(swiftyData)
                
                if swiftyData["status"].stringValue == "true"{
                    
                    let alert = UIAlertController(title: AppName, message: "Product deleted succesfully.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
                        self.tableView.reloadData()
                    }))
                    
                    
                    self.present(alert, animated: true, completion:nil)
                    
                    
                    
                }else{
                    
                    
                }
            }
        }
        
    }
    
}

