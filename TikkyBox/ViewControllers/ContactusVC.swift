//
//  ContactusVC.swift
//  TikkyBox
//
//  Created by Sandeep Kumar on 29/05/2020.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit
import WebKit

class ContactusVC: UIViewController,WKNavigationDelegate,UITextFieldDelegate {
    
    
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let url = URL(string: "https://webmobril.org/dev/tikkybox/api/pages/contact")
        let requestObj = URLRequest(url: url! as URL)
        webView.navigationDelegate = self
        webView.uiDelegate = self as? WKUIDelegate
        webView.scrollView.bounces = false
        webView.load(requestObj)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
            MBProgressHUD.hideHUD()
        MBProgressHUD.hide(for: (self.view)!, animated: true)
    }
    
    private func webView(webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: NSError) {
    MBProgressHUD.hideHUD()
    MBProgressHUD.hide(for: (self.view)!, animated: true)
    }
}
