//
//  AddStoreVC.swift
//  TikkyBox
//
//  Created by Sandeep Kumar on 01/07/2020.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class AddStoreVC: UIViewController ,UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    
    @IBOutlet var views: [UIView]!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtType: UITextField!
    @IBOutlet weak var plusBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var imageLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    var storeDict = JSON()
    
    var isUploadImage = false
    
    var imagePicker = UIImagePickerController()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        setUI()
        
    }
    
    func setUI(){
        
        imagePicker.delegate = self
        
        saveBtn.layer.masksToBounds = true
        saveBtn.layer.cornerRadius = 5.0
        
        imgView.layer.masksToBounds = true
        imgView.layer.cornerRadius = 5.0
        imgView.layer.borderColor = UIColor.orange.cgColor
        imgView.layer.borderWidth = 2.0
        
        for item in views{
            
            item.layer.cornerRadius = 10
            item.layer.shadowColor = UIColor.lightGray.cgColor
            item.layer.shadowOpacity = 1
            item.layer.shadowOffset = CGSize.zero
            item.layer.shadowRadius = 2.5
            
        }
        
        
        if storeDict.count > 0{
            isUploadImage = true
            
            txtName.text = storeDict["store_name"].stringValue
            txtPhone.text = storeDict["store_phone"].stringValue
            txtType.text = storeDict["store_type"].stringValue
            txtAddress.text = storeDict["store_address"].stringValue

            let imagename = "\(GlobalURL.imagebaseURL)\(storeDict["store_image"].stringValue)"
            
            let urlString = imagename.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            
            let imgURL = URL(string: urlString )
            
            imgView?.af_setImage(withURL: imgURL!, placeholderImage:  UIImage(named: "sample_icon3"))
            self.titleLabel.text = "Store Details"
            saveBtn.setTitle("Update Store", for: .normal)
        }else{
            self.titleLabel.text = "Add Store"
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func isValidation()->Bool{
        if !(txtName.text?.isValidTextField())!{
            self.createAlert(title: AppName, message: "Enter Store name.")
            return false
        }else if !(txtPhone.text?.isValidTextField())!{
            self.createAlert(title: AppName, message: "Enter Store phone number.")
            return false
        }else if !(txtPhone.text?.isValidPhoneNumber())!{
            createAlert(title: AppName, message: validate_mob)
            return false
        }else if !(txtAddress.text?.isValidTextField())!{
            self.createAlert(title: AppName, message: "Enter Store location.")
            return false
        }else if !(txtType.text?.isValidTextField())!{
            self.createAlert(title: AppName, message: "Enter Store type.")
            return false
        }else if isUploadImage == false {
            self.createAlert(title: AppName, message: "Upload Store Image.")
            return false
        }
        
        
        return true
    }
    
    
    @IBAction func addAction(_ sender: Any) {
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in self.openGallary()
            
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as? UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            
            self.createAlert(title: AppName, message: "You don't have camera")
            
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.modalPresentationStyle = .fullScreen
        self.present(imagePicker, animated: true, completion: nil)
        
    }
    
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        imgView.image = (info[UIImagePickerController.InfoKey.originalImage] as! UIImage)
        self.imageLabel.textColor = UIColor.blue
        self.imageLabel.text = "Uploaded"
        self.isUploadImage = true
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func saveAction(_ sender: UIButton) {
        
        if isValidation(){
            if sender.currentTitle == "Update Store"{
                
                updateStore()
                
            }else{
            
            addStoreMethod()
            }
        }
    }
    
    
    func addStoreMethod(){
             
          
                
                let baseURL = "\(GlobalURL.baseURL3)\(GlobalURL.storeURL)"
                
                print(baseURL)
                let userId = UserDefaults.standard.getUserId()
                
        let params = ["auth":"\(userId)","store_name":"\(txtName.text ?? "")","store_phone":"\(txtPhone.text ?? "")","store_type":"\(txtType.text ?? "")","store_address":"\(txtAddress.text ?? "")"]
                
        let imgData = self.imgView.image?.jpegData(compressionQuality: 0.4)
                
                print("Login Parameters:\(params)")
                MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
                
                Alamofire.upload(multipartFormData: { multipartFormData in
    //                DispatchQueue.main.async {
                        multipartFormData.append(imgData!, withName: "store_image",fileName: "store_image.jpg", mimeType: "image/jpg")
    //                }
                    
                    for (key, value) in params {
                        multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                    }
                }, to:baseURL) { (result) in
                    
                    switch result {
                    case .success(let upload, _, _):
                        upload.uploadProgress(closure: { (progress) in
                            
                            print(progress.fractionCompleted)
                            
                        })
                        upload.responseJSON { response in
                            
                            
                            MBProgressHUD.hide(for: self.view, animated: true)
                            
                            
                            if (response.result.value != nil){
                                
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                
                                
                                if swiftyJsonVar["status"] == true {
                                    
                                     let alert = UIAlertController(title: AppName, message: "Store created succesfully.", preferredStyle: .alert)
                                           alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
                                            self.navigationController?.popViewController(animated: true)
                                           }))
                                          
                                    
                                    self.present(alert, animated: true, completion:nil)
                                    
                                } else {
                                    
                                    self.createAlert(title: AppName, message: swiftyJsonVar["message"].stringValue)
                                    
                                    
                                }
                            }
                        }
                        
                    case .failure(let encodingError):
                        print(encodingError)
                        
                    }
                    
                }
                
            
        }
    
    
    func updateStore(){
             
          
                
                let baseURL = "\(GlobalURL.baseURL3)\(GlobalURL.updateStoreURL)"
                
                print(baseURL)
                let userId = UserDefaults.standard.getUserId()
                
        let params = ["auth":"\(userId)","store_id":"\(storeDict["id"].stringValue)","store_name":"\(txtName.text ?? "")","store_phone":"\(txtPhone.text ?? "")","store_type":"\(txtType.text ?? "")","store_address":"\(txtAddress.text ?? "")"]
                
        let imgData = imgView.image?.jpegData(compressionQuality: 0.4)
                
                print("Login Parameters:\(params)")
                MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
                
                Alamofire.upload(multipartFormData: { multipartFormData in
    //                DispatchQueue.main.async {
                        multipartFormData.append(imgData!, withName: "store_image",fileName: "store_image.jpg", mimeType: "image/jpg")
    //                }
                    
                    for (key, value) in params {
                        multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                    }
                }, to:baseURL) { (result) in
                    
                    switch result {
                    case .success(let upload, _, _):
                        upload.uploadProgress(closure: { (progress) in
                            
                            print(progress.fractionCompleted)
                            
                        })
                        upload.responseJSON { response in
                            
                            
                            MBProgressHUD.hide(for: self.view, animated: true)
                            
                            
                            if (response.result.value != nil){
                                
                                let swiftyJsonVar = JSON(response.result.value!)
                                
                                
                                
                                if swiftyJsonVar["status"] == true {
                                    
                                     let alert = UIAlertController(title: AppName, message: "Store updated succesfully.", preferredStyle: .alert)
                                           alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
                                            self.navigationController?.popViewController(animated: true)
                                           }))
                                          
                                    
                                    self.present(alert, animated: true, completion:nil)
                                    
                                } else {
                                    
                                    self.createAlert(title: AppName, message: swiftyJsonVar["message"].stringValue)
                                    
                                    
                                }
                            }
                        }
                        
                    case .failure(let encodingError):
                        print(encodingError)
                        
                    }
                    
                }
                
            
        }
    
}
