//
//  SelectCategoryVC.swift
//  TikkyBox
//
//  Created by Himanshu on 13/03/20.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit
import Alamofire
import ListPlaceholder
import AlamofireImage
import Foundation
import SwiftyJSON

class SelectCategoryVC: UIViewController,UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collCategory: UICollectionView!
    @IBOutlet weak var btnContinue: UIButton!
    
    
    
   var index = IndexPath(row: 0, section: 0)
    var category = [JSON]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        btnContinue.makeRoundCorner(8)
        
        getCategoryList()
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            
            return category.count
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
            let firstSelectCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CpnCategoryCollCell", for: indexPath) as! CpnCategoryCollCell
            
            firstSelectCell.containerView.isUserInteractionEnabled = true
          
            if index.row ==  indexPath.row{
                firstSelectCell.containerView.backgroundColor = .baseRed
                firstSelectCell.lblPromoTyp.textColor = .white
                
            }else{
                firstSelectCell.containerView.backgroundColor = .white
                firstSelectCell.lblPromoTyp.textColor = .baseRed
            }
            
            firstSelectCell.lblPromoTyp.text = category[indexPath.row]["name"].stringValue
            
            let imagename = "\(GlobalURL.imagebaseURL)\(category[indexPath.row]["image"].stringValue)"
            
            let urlString = imagename.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            
            let imgURL = URL(string: urlString )
            
            firstSelectCell.imgPromo?.af_setImage(withURL: imgURL!, placeholderImage:  UIImage(named: "sample_icon3"))

            return firstSelectCell
            
        }
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            
            index = indexPath
            
            
            
            collCategory.reloadData()

        }
        
        
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            
            let cvw = collectionView.frame.size.width - 30
    //        let cvh = collectionView.frame.size.height
            return CGSize(width: cvw/2, height: 160)
            
        }
        
        
        
        func getCategoryList(){
            
            
            let baseUrl = "\(GlobalURL.baseURL2)\(GlobalURL.categoryURL)"
            
            
            
            MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
            
            Alamofire.request(baseUrl, method: .get).responseJSON { response in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                if response.result.value != nil{
                    let swiftyData = JSON(response.result.value!)
                    
                    print(swiftyData)
                    
                    if swiftyData["result"]["status"].stringValue == "true"{
                        
                        self.category = swiftyData["result"]["categories"].arrayValue
                        self.btnContinue.isHidden = false
                        DispatchQueue.main.async {
                             self.collCategory.reloadData()
                            
                        }
                       
                        
                       
                    }else{
                        
                        
                    }
                }
            }
            
        }
        
    
    


    
    
    @IBAction func tapOnContinue(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
               let vc = storyboard.instantiateViewController(identifier: "CustomerHomeVC") as! CustomerHomeVC
        
//        vc.categoryId = category[index.row]["id"].stringValue
        UserDefaults.standard.setCategoryId(categoryId: category[index.row]["id"].stringValue)
               self.navigationController?.pushViewController(vc, animated: true)
    }
    
}



