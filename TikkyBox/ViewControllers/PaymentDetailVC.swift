//
//  PaymentDetailVC.swift
//  TikkyBox
//
//  Created by Sandeep Kumar on 25/06/2020.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class PaymentDetailVC: UIViewController {
    
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var txtIFSC: UITextField!
    @IBOutlet weak var txtBank: UITextField!
    @IBOutlet weak var txtAccount: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet var commonView: [UIView]!
    @IBOutlet weak var submitBtn: UIButton!
    var dataDict = JSON()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        getPaymentDetails()
    }
    
    func setUI(){
        scrollView.isHidden = false
        submitBtn.layer.masksToBounds = true
               submitBtn.layer.cornerRadius = 5.0
               
               for item in commonView{
               
               item.layer.cornerRadius = 10
               item.layer.shadowColor = UIColor.lightGray.cgColor
               item.layer.shadowOpacity = 1
               item.layer.shadowOffset = CGSize.zero
               item.layer.shadowRadius = 2.5

               }
        
        if dataDict.count > 0{
            txtName.text = dataDict["account_holder_name"].stringValue
            txtAccount.text = dataDict["account_number"].stringValue
            txtBank.text = dataDict["bank_name"].stringValue
            txtIFSC.text = dataDict["ifsc_code"].stringValue
            submitBtn.setTitle("Update", for: .normal)
        }
    }
    

     @IBAction func backAction(_ sender: Any) {
           self.navigationController?.popViewController(animated: true)
       }
    
    @IBAction func submitAction(_ sender: UIButton) {
        if isValidation(){
            if sender.currentTitle == "Update"{
                updatePaymentDetail()
            }else{
            addPaymentMethod()
            }
        }
    }
    
    func isValidation()->Bool{
        if !(txtName.text?.isValidTextField())!{
            self.createAlert(title: AppName, message: "Enter Account holder name.")
            return false
        }else if !(txtAccount.text?.isValidTextField())!{
            self.createAlert(title: AppName, message: "Enter Account number.")
            return false
        }else if txtAccount.text!.count < 12 || txtAccount.text!.count > 16 {
            self.createAlert(title: AppName, message: "Please enter valid account number(12-16 digits).")
            return false
        }else if !(txtBank.text?.isValidTextField())!{
            self.createAlert(title: AppName, message: "Enter Bank name.")
            return false
        }else if !(txtIFSC.text?.isValidTextField())!{
            self.createAlert(title: AppName, message: "Enter IFSC Code.")
            return false
        }else if !(txtIFSC.text?.isValidALPHANUMBER())! {
            self.createAlert(title: AppName, message: "IFSC Code should be 11 characters long alhpa numeric.")
            return false
        }else if txtIFSC.text!.count > 11 || txtIFSC.text!.count < 11 {
            self.createAlert(title: AppName, message: "IFSC Code should be 11 characters long alhpa numeric.")
            return false
        }
        
        
        return true
    }
    
    
    func addPaymentMethod(){
        /*account_holder_name:sandeep
        account_number:1234566
        bank_name:abc
        ifsc_code:ahc1223
        auth:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjY2In0.dE-jdko2TQ5wyIfLvxtrThJTnS3apqlmh6F8OhNKzEc*/
        
        let baseUrl = "\(GlobalURL.baseURL2)\(GlobalURL.addPaymentDetailURL)"
        
        let userId = UserDefaults.standard.getUserId()
        let params = ["auth":"\(userId)","account_holder_name":"\(txtName.text ?? "")","account_number":"\(txtAccount.text ?? "")","ifsc_code":"\(txtIFSC.text ?? "")","bank_name":"\(txtBank.text ?? "")"]
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseUrl, method: .post,parameters: params).responseJSON { response in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            if response.result.value != nil{
                let swiftyData = JSON(response.result.value!)
                
                print(swiftyData)
                
                if swiftyData["status"].stringValue == "true"{
                    
                    let alert = UIAlertController(title: AppName, message: "Account added succesfully.", preferredStyle: .alert)
                           alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {_ in
                            self.navigationController?.popViewController(animated: true)
                           }))
                          
                    
                    self.present(alert, animated: true, completion:nil)
                    
                    
                }else{
                    
                    
                }
            }
        }
        
    }
    
    func updatePaymentDetail(){
        /*account_holder_name:sandeep
        account_number:1234566
        bank_name:abc
        ifsc_code:ahc1223
        auth:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjY2In0.dE-jdko2TQ5wyIfLvxtrThJTnS3apqlmh6F8OhNKzEc*/
        
        let baseUrl = "\(GlobalURL.baseURL2)\(GlobalURL.updatePaymentDetailURL)"
        
        let userId = UserDefaults.standard.getUserId()
        let params = ["auth":"\(userId)","account_holder_name":"\(txtName.text ?? "")","account_number":"\(txtAccount.text ?? "")","ifsc_code":"\(txtIFSC.text ?? "")","bank_name":"\(txtBank.text ?? "")","payment_id":dataDict["id"].stringValue]
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseUrl, method: .post,parameters: params).responseJSON { response in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            if response.result.value != nil{
                let swiftyData = JSON(response.result.value!)
                
                print(swiftyData)
                
                if swiftyData["status"].stringValue == "true"{
                    
                    let alert = UIAlertController(title: AppName, message: "Account updated succesfully.", preferredStyle: .alert)
                           alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {_ in
                            self.navigationController?.popViewController(animated: true)
                           }))
                          
                    
                    self.present(alert, animated: true, completion:nil)
                    
                    
                }else{
                    
                    
                }
            }
        }
        
    }
    
    
    func getPaymentDetails(){
        
        
        let baseUrl = "\(GlobalURL.baseURL2)\(GlobalURL.addPaymentDetailURL)"
        
        let userId = UserDefaults.standard.getUserId()
        let params = ["auth":"\(userId)"]
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseUrl, method: .get,parameters: params).responseJSON { response in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            if response.result.value != nil{
                let swiftyData = JSON(response.result.value!)
                
                print(swiftyData)
                
                if swiftyData["status"].stringValue == "true"{
                    
                    self.dataDict = swiftyData["result"]
                    self.setUI()
                    
                    
                }else{
                    
                    self.setUI()
                }
            }
        }
        
    }

}
