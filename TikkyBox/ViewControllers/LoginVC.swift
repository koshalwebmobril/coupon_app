//
//  LoginVC.swift
//  TikkyBox
//
//  Created by Himanshu on 05/03/20.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import GoogleSignIn
import FacebookCore
import FBSDKCoreKit
import FBSDKLoginKit
import CountryPickerView


class LoginVC: UIViewController, GIDSignInDelegate,UITextFieldDelegate {
    
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var txtMobNo: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    
    lazy var uiComponent = [view1 , view2 , view3 , view4]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().presentingViewController = self
        
//        txtMobNo.text = "9205720034"
//        txtPassword.text = "1234567"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        txtMobNo.text = ""
                txtPassword.text = ""
//        txtMobNo.becomeFirstResponder()
    }
    
    func setupUI()
    {
        view1.makeBorder(1)
        view2.makeBorder(1)
        uiComponent.forEach { (setup) in
            setup?.makeRoundCorner(8)
        }
        btnLogin.makeRoundCorner(8)
//                let cpv = CountryPickerView(frame: CGRect(x: 0, y: 0, width: 33, height: 20))
//                cpv.showCountryCodeInView = false
//                cpv.setCountryByName("India")
//                      cpv.setCountryByCode("IN")
//                       cpv.setCountryByPhoneCode("+91")
//                       txtMobNo.leftView = cpv
//
//                       txtMobNo.leftViewMode = .always
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //  Validation
    
    private func validation() -> Bool
    {
        guard let mobile = txtMobNo.text ,  !mobile.isEmpty else {
            createAlert(title: AppName, message: valid_Mob)
            return false
        }
        guard let password = txtPassword.text ,  !password.isEmpty else {
            createAlert(title: AppName, message: valid_password)
            return false
        }
        guard let passStrong = txtPassword.text ,  passStrong.count > 6 else {
            createAlert(title: AppName, message: valid_pass_strng)
            return false
        }
        apiLogin()
        return true
    }
    
    // Api
    
    private func apiLogin()
    {
        
        /*device_type
        device_token*/
        
        let parameters: [String: Any] = [
            "mobile" : txtMobNo.text!,
            "password" : txtPassword.text!,
            "device_type": "2",
            "device_token": "123"
        ]
        
        let baseUrl =  "\(GlobalURL.baseURL)\(GlobalURL.loginURL)"
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseUrl, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON { response in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                print(response)
                if response.result.value != nil{
                    let swiftyData = JSON(response.result.value!)
                    if swiftyData["status"].stringValue == "true"{
                        UserDefaults.standard.setUserType(type: swiftyData["result"]["role"].stringValue)
                        UserDefaults.standard.setUserId(userId: swiftyData["result"]["jwt_token"].stringValue)
                        if swiftyData["result"]["role"].stringValue == "Customer"{
                        
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewController(identifier: "SelectCategoryVC") as! SelectCategoryVC
                        self.navigationController?.pushViewController(vc, animated: true)
                        }else{
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.gotoRetailerHomeVC()
                        }
                    }else{
                        self.createAlert(title: AppName, message: swiftyData["message"].stringValue)
                    }
                }else{
                    self.createAlert(title: AppName, message: "Something went wrong")
                }
               
                
                
        }
    }
    
    @IBAction func tapOnLogin(_ sender: Any) {
           validation()

    }
    
    @IBAction func tapOnForgotPass(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "ForgotPasswordVC") as! ForgotPasswordVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    // Facebook SignIn
    @IBAction func tapOnFbLogin(_ sender: Any) {
        createAlert(title: AppName, message: "Development is on progress...")
//        let fbLoginManager : LoginManager = LoginManager()
//        fbLoginManager.logIn(permissions: ["email"], from: self) {   (result, error) in
//            if (error == nil){
//                let fbloginresult : LoginManagerLoginResult = result!
//                if fbloginresult.grantedPermissions != nil {
//                    if(fbloginresult.grantedPermissions.contains("email"))
//                    {
//                        self.getFBUserData()
//                        fbLoginManager.logOut()
//                    }
//                }
//            }
//        }
    }
    
    
    func getFBUserData(){
        if((AccessToken.current) != nil){
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email, about"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    print(result!)
                    if let data = result as? [String:Any]{
                        print(data)
                        let swiftyData = JSON(data)
                        print("Swifty: \(swiftyData)")
                    
                    
                        self.socialLogin(loginType: "facebook", fname: swiftyData["first_name"].stringValue, lname: swiftyData["last_name"].stringValue, email: swiftyData["email"].stringValue)
                    }
                }
            })
        }
    }
    
    
    @IBAction func tapOnGoogleLogin(_ sender: Any) {
        createAlert(title: AppName, message: "Development is on progress...")
        
//        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func tapOnRegister(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "RegisterVC") as! RegisterVC
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    
    
    // Google SignIN
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                print("The user has not signed in before or they have since signed out.")
            } else {
                print("\(error.localizedDescription)")
            }
            return
        }
        // Perform any operations on signed in user here.
        
        var firstName,lastName,emailId : String!
        
        let userId = user.userID                  // For client-side use only!
        let idToken = user.authentication.idToken // Safe to send to the server
        let fullName = user.profile.name
        if let givenName = user.profile.givenName{
            firstName = givenName
        }
        if let familyName = user.profile.familyName{
            lastName = familyName
        }
        if let email = user.profile.email{
            emailId = email
        }
        
        print("This is data get by gmail \(userId) & \(idToken) & \(fullName) & \(firstName) & \(lastName) & \(emailId)")
        self.socialLogin(loginType: "google", fname: firstName, lname: lastName, email: emailId)
        // ...
    }
    
    
    
    
    // Start Google OAuth2 Authentication
    func sign(_ signIn: GIDSignIn?, present viewController: UIViewController?) {
        
        // Showing OAuth2 authentication window
        if let aController = viewController {
            present(aController, animated: true) {() -> Void in }
        }
    }
    // After Google OAuth2 authentication
    func sign(_ signIn: GIDSignIn?, dismiss viewController: UIViewController?) {
        // Close OAuth2 authentication window
        dismiss(animated: true) {() -> Void in }
    }
    
    private func socialLogin(loginType:String,fname:String,lname:String,email:String){
        
        /*device_type
        device_token*/
        
        let parameters: [String: Any] = [
            "username" : "\(fname) \(lname)",
            "email" : "\(email)",
            "oauth_provider" : "\(loginType)",
            "device_type": "2",
            "device_token": "123"
        ]
        
        let baseUrl =  "\(GlobalURL.baseURL)\(GlobalURL.socialLoginURL)"
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseUrl, method: .post, parameters: parameters )
            .responseJSON { response in
                
                MBProgressHUD.hide(for: self.view, animated: true)
                print(response)
                if response.result.value != nil{
                    let swiftyData = JSON(response.result.value!)
                    if swiftyData["status"].stringValue == "true"{
                        UserDefaults.standard.setUserType(type: swiftyData["result"]["role"].stringValue)
                        UserDefaults.standard.setUserId(userId: swiftyData["result"]["jwt_token"].stringValue)
                        if swiftyData["result"]["role"].stringValue == "Customer"{
                        
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewController(identifier: "SelectCategoryVC") as! SelectCategoryVC
                        self.navigationController?.pushViewController(vc, animated: true)
                        }else{
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.gotoRetailerHomeVC()
                        }
                    }else{
                        self.createAlert(title: AppName, message: swiftyData["message"].stringValue)
                    }
                }else{
                    self.createAlert(title: AppName, message: "Something went wrong")
                }
               
                
                
        }
    }
    
    
    
}

