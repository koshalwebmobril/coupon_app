//
//  ProductPopupVC.swift
//  TikkyBox
//
//  Created by Sandeep Kumar on 08/07/2020.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

protocol AddProductDelegate {
    func addProduct(data:[String],ids:[String],indexs:[IndexPath])
}

class ProductPopupVC: UIViewController ,UITableViewDelegate,UITableViewDataSource{
    
    

    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var frontView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var submitBtn: UIButton!
    var delegate : AddProductDelegate!
    
    var isFromRedeem = false
    
    var selectedIndex = [IndexPath]()
    
    var productSelected = [String]()
    
    var productArray = [JSON]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        submitBtn.layer.masksToBounds = true
        submitBtn.layer.cornerRadius = 5.0
        
        frontView.layer.masksToBounds = true
        frontView.layer.cornerRadius = 5.0
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell", for: indexPath) as! ProductCell
        cell.titleLabel.text = productArray[indexPath.row]["product_name"].stringValue
        
        cell.checkBtn.isHidden = true
        cell.uncheckBtn.isHidden = false
        
        for item in selectedIndex{
            if indexPath.row == item.row{
                cell.checkBtn.isHidden = false
                cell.uncheckBtn.isHidden = true
            } 
        }
        return cell
    }

    @IBAction func checkAction(_ sender: UIButton) {
        
        let position = sender.convert(CGPoint.zero, to: tableView)
        let indexPath = tableView.indexPathForRow(at: position)!
        let cell = sender.superview?.superview as! ProductCell
        
        cell.checkBtn.isHidden = true
        cell.uncheckBtn.isHidden = false
        
        for (index,item) in selectedIndex.enumerated(){
            if item.row == indexPath.row{
                selectedIndex.remove(at: index)
            }
        }
        

        
        
        
        
    }
    
    
    @IBAction func uncheckAction(_ sender: UIButton) {
        
        let position = sender.convert(CGPoint.zero, to: tableView)
        let indexPath = tableView.indexPathForRow(at: position)!
        
        let cell = sender.superview?.superview as! ProductCell
        
        cell.checkBtn.isHidden = false
        cell.uncheckBtn.isHidden = true
        
        selectedIndex.append(indexPath)
        

    }
    
    
    
    @IBAction func doneAction(_ sender: UIButton) {
        
        if isFromRedeem{
            self.dismiss(animated: true, completion: nil)
            
        }else{
        
        var dataArray = [String]()
        var dataId = [String]()
        selectedIndex.forEach { (IndexPath) in
            let data = self.productArray[IndexPath.row]["product_name"].stringValue
            let id = self.productArray[IndexPath.row]["id"].stringValue
            dataArray.append(data)
            dataId.append(id)
        }
        
        delegate.addProduct(data: dataArray, ids: dataId, indexs: selectedIndex)
        self.dismiss(animated: true, completion: nil)
    }
    }
     
        
    
}
