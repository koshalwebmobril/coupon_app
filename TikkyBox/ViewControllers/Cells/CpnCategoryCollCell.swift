//
//  CpnCategoryCollCell.swift
//  TikkyBox
//
//  Created by Himanshu on 05/03/20.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit

class CpnCategoryCollCell: UICollectionViewCell {
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var imgPromo: UIImageView!
    @IBOutlet weak var lblPromoTyp: UILabel!
    
    override func awakeFromNib() {
        containerView.makeRoundCorner(8)
        containerView.dropShadow()
    }
}
