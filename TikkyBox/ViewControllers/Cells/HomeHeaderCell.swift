//
//  HomeHeaderCell.swift
//  TikkyBox
//
//  Created by Himanshu on 13/03/20.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit

class HomeHeaderCell: UITableViewCell {
    
    
    @IBOutlet weak var headerLabel: UILabel!
    
    @IBOutlet weak var viewMoreBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
