//
//  TopTypeCollCell.swift
//  TikkyBox
//
//  Created by Himanshu on 05/03/20.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit

class TopTypeCollCell: UICollectionViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblCategory: UILabel!
    
    override func awakeFromNib() {
        containerView.makeRoundCorner(5)
        containerView.dropShadow()
    }
    
}
