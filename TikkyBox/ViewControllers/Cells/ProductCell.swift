//
//  ProductCell.swift
//  TikkyBox
//
//  Created by Himanshu on 05/03/20.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit

class ProductCell: UITableViewCell {

    
    @IBOutlet weak var imgBanner: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblCpnName: UILabel!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblCpnExpry: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var checkBtn: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblStore: UILabel!
    @IBOutlet weak var uniqueCode: UILabel!
    
    @IBOutlet weak var uncheckBtn: UIButton!
    
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblBusiness: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblLink: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
