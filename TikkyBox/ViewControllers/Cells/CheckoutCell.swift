//
//  CheckoutCell.swift
//  TikkyBox
//
//  Created by Himanshu on 05/03/20.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit

class CheckoutCell: UITableViewCell {

    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var stckView: UIStackView!
    @IBOutlet weak var coverView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        coverView.makeRoundCorner(5)
        coverView.makeBorder(1)
        
        containerView.makeRoundCorner(8)
        containerView.dropShadow()
        stckView.addBackground(color: .red)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
