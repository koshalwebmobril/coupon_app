//
//  ReportCell.swift
//  TikkyBox
//
//  Created by Sandeep Kumar on 18/05/2020.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit

class ReportCell: UITableViewCell {
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var savingLabel: UILabel!
    @IBOutlet weak var savingPrice: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bgView.makeRoundCorner(5)
        bgView.dropShadow()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
