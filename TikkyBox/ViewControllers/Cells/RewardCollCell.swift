//
//  RewardCollCell.swift
//  TikkyBox
//
//  Created by Himanshu on 05/03/20.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit

class RewardCollCell: UICollectionViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imgPromo: UIImageView!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var lblCpnWon: UILabel!
    @IBOutlet weak var expDate: UILabel!
    
    @IBOutlet weak var fadeView: UIView!
    @IBOutlet weak var claimBtn: UIButton!
    
    override func awakeFromNib() {
        setupUI()
    }
    
    func setupUI()
    {
        containerView.makeRoundCorner(8)
        viewBottom.makeRoundCorner(14)
    }
    
}
