//
//  CouponTikkyCell.swift
//  TikkyBox
//
//  Created by Himanshu on 05/03/20.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit

class CouponTikkyCell: UITableViewCell {

    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblCpnTitle: UILabel!
    @IBOutlet weak var lblStoreName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblCutPrice: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var lblExpireDate: UILabel!
    @IBOutlet weak var btnRemove: UIButton!
    @IBOutlet weak var btnView: UIButton!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!

    
    
    
    lazy var uiComponent = [view1 , view2]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        // Initialization code
    }
    

    func setupUI()
    {
        uiComponent.forEach { (setup) in
            setup?.makeRoundCorner(5)
            setup?.dropShadow()
        }
        view3.layer.cornerRadius = 5
        view3.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMinXMinYCorner]
        view4.layer.cornerRadius = 5
        view4.layer.maskedCorners = [ .layerMaxXMinYCorner , .layerMaxXMaxYCorner ]
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

