//
//  ForgotPasswordVC.swift
//  TikkyBox
//
//  Created by Himanshu on 14/03/20.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ForgotPasswordVC: UIViewController,UITextFieldDelegate {
    
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var txtContactNo: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPasswrd: UITextField!
    
    lazy var uiComponent = [view1 , view2 , view3]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
    }
    
    func setupUI()
    {
        
        uiComponent.forEach { (setup) in
            setup?.makeBorder(1)
            setup?.makeRoundCorner(8)
        }
        btnSubmit.makeRoundCorner(8)
    }
    
   
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    private func validation() -> Bool
    {
        guard let mobile = txtContactNo.text ,  !mobile.isEmpty else {
            createAlert(title: AppName, message: valid_Mob)
            return false
        }
        guard let password = txtPassword.text ,  !password.isEmpty else {
            createAlert(title: AppName, message: valid_password)
            return false
        }
        guard let passStrong = txtPassword.text ,  passStrong.count > 6 else {
            createAlert(title: AppName, message: valid_pass_strng)
            return false
        }
        guard let confirmPassword = txtConfirmPasswrd.text ,  !confirmPassword.isEmpty else {
            createAlert(title: AppName, message: valid_Confirm)
            return false
        }
        guard let confirmedPassword = txtConfirmPasswrd.text ,  confirmedPassword == password else {
            createAlert(title: AppName, message: valid_ConfirmPassword)
            return false
        }
        
        apiForgotPassword()
        return true
    }
    
    
    private func apiForgotPassword()
    {
        let parameters: [String: Any] = [
            "mobile" : txtContactNo.text!,
            "password" : txtConfirmPasswrd.text!
        ]
        
        let baseUrl = "\(GlobalURL.baseURL)\(GlobalURL.forgotPasswordURL)"
        
        print("This is param \(parameters)")
        
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        Alamofire.request(baseUrl, method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON { response in
                MBProgressHUD.hide(for: self.view, animated: true)
                print(response)
                if  response.result.value != nil{
                    let swiftyData = JSON(response.result.value!)
                    
                    print(swiftyData)
                    
                    if swiftyData["status"].stringValue == "true"{
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                                let vc = storyboard.instantiateViewController(identifier: "OtpVerifyVC") as! OtpVerifyVC
                                                vc.mobileNo = self.txtContactNo.text!
                                                vc.via_forgot = 1
                                             
                        vc.otpStr = swiftyData["result"]["otp"].stringValue
                                                self.navigationController?.pushViewController(vc, animated: true)
                                        
                    }else{
                        self.createAlert(title: AppName, message: swiftyData["message"].stringValue)
                    }
                    
                }
                else
                {
                    self.createAlert(title: AppName, message: "Something went wrong")
                }
                
                
        }
    }
    
    @IBAction func tapOnSubmit(_ sender: Any) {
        validation()
    }
    
    @IBAction func tapOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
