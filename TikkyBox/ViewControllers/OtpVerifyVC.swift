//
//  OtpVerifyVC.swift
//  TikkyBox
//
//  Created by Himanshu on 05/03/20.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class OtpVerifyVC: UIViewController {
    
    @IBOutlet weak var txtMobileNo: UILabel!
    @IBOutlet weak var txtOtp1: UITextField!
    @IBOutlet weak var txtOtp2: UITextField!
    @IBOutlet weak var txtOtp3: UITextField!
    @IBOutlet weak var txtOtp4: UITextField!
    @IBOutlet weak var btnVerify: UIButton!
    
    lazy var txtComponent = [txtOtp1 ,txtOtp2 , txtOtp3, txtOtp4 ]
    var mobileNo = String()
    var via_forgot = Int()
    
    var otpStr = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let last4 = mobileNo.suffix(4)
        txtMobileNo.text = "*******\(last4)"
        print("This is our mobileNo \(last4)")
//        txtMobileNo.text = "\(mobileNo)"
        setupUI()
        
        var count = 0
        
        for word in  self.otpStr{
            print(word)
            if count == 0{
                self.txtOtp1.text = "\(word)"
                count = count + 1
            }else if count == 1{
                self.txtOtp2.text = "\(word)"
                count = count + 1
            }else if count == 2{
                self.txtOtp3.text = "\(word)"
                count = count + 1
            }else {
                self.txtOtp4.text = "\(word)"
                count = count + 1
             
            }
            
        }
        
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupUI()
    {
        txtComponent.forEach { (setup) in
            setup?.delegate = self
        }
        btnVerify.makeRoundCorner(8)
    }
    
    private func validate() -> Bool
    {
        guard let otp1 = txtOtp1.text , !otp1.isEmpty else {
            createAlert(title: AppName, message: valid_otp)
            return false
        }
        guard let otp2 = txtOtp2.text , !otp2.isEmpty else {
            createAlert(title: AppName, message: valid_otp)
            return false
        }
        guard let otp3 = txtOtp3.text , !otp3.isEmpty else {
            createAlert(title: AppName, message: valid_otp)
            return false
        }
        guard let otp4 = txtOtp4.text , !otp4.isEmpty else {
            createAlert(title: AppName, message: valid_otp)
            return false
        }
        return true
    }
    
    
   
    
    
    @IBAction func tapOnReset(_ sender: Any) {
       resendOtp()
        
    }
    
    @IBAction func tapOnVerify(_ sender: Any) {
        if validate(){
            verifyOtp()
        }
        
    }
    
    
    
    
    func verifyOtp(){
        /*otp
        mobile
        forgot_pass*/
        
        let baseUrl = "\(GlobalURL.baseURL)\(GlobalURL.verifyOtpURL)"
        
        let optStr = "\(txtOtp1.text!)\(txtOtp2.text!)\(txtOtp3.text!)\(txtOtp4.text!)"
        
        var forgot = 0
        if self.via_forgot == 1{
            forgot = 1
        }else{
            forgot = 0
        }
        
        let params :[String:Any] = ["otp":"\(optStr)" ,"mobile":mobileNo,"forgot_pass":"\(forgot)"]
        
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseUrl, method: .post, parameters: params, encoding: URLEncoding.default , headers: nil).responseJSON { response in
            MBProgressHUD.hide(for: self.view, animated: true)
            if response.result.value != nil{
                let swiftyData = JSON(response.result.value!)
                
                if swiftyData["status"].stringValue == "true"{
                    if self.via_forgot == 1{
                        let alert = UIAlertController(title: AppName, message: "Password changed successfully.", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: {_ in
                            self.navigationController?.popToRootViewController(animated: false)
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }else{
                        UserDefaults.standard.setUserType(type: swiftyData["result"]["role"].stringValue)
                        UserDefaults.standard.setUserId(userId: swiftyData["result"]["jwt_token"].stringValue)
                        if swiftyData["result"]["role"].stringValue == "Customer"{
                        
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewController(identifier: "SelectCategoryVC") as! SelectCategoryVC
                        self.navigationController?.pushViewController(vc, animated: true)
                        }else{
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.gotoRetailerHomeVC()
                        }
                    }
                }else{
                    
                    self.createAlert(title: AppName, message: swiftyData["message"].stringValue)
                }
            }
        }
        
    }
    
    
    func resendOtp(){
        /*otp
        mobile
        forgot_pass*/
        
        let baseUrl = "\(GlobalURL.baseURL)\(GlobalURL.resendOtpURL)"
        
        let params :[String:Any] = ["mobile":mobileNo]
        
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseUrl, method: .post, parameters: params, encoding: URLEncoding.default , headers: nil).responseJSON { response in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            if response.result.value != nil{
                let swiftyData = JSON(response.result.value!)
                print(swiftyData)
                if swiftyData["status"].stringValue == "true"{
                    let otp = swiftyData["result"]["otp"].stringValue
                    var count = 0
                    for word in  otp{
                               print(word)
                               if count == 0{
                                   self.txtOtp1.text = "\(word)"
                                   count = count + 1
                               }else if count == 1{
                                   self.txtOtp2.text = "\(word)"
                                   count = count + 1
                               }else if count == 2{
                                   self.txtOtp3.text = "\(word)"
                                   count = count + 1
                               }else {
                                   self.txtOtp4.text = "\(word)"
                                   count = count + 1
                                
                               }
                               
                           }
                    
                    self.createAlert(title: AppName, message: "OTP resend successfully")
                }else{
                    self.createAlert(title: AppName, message: swiftyData["message"].stringValue)
                }
            }
        }
        
    }
    
}





extension OtpVerifyVC: UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (string.count == 1){
            if textField == txtOtp1 {
                txtOtp2?.becomeFirstResponder()
            }
            if textField == txtOtp2 {
                txtOtp3?.becomeFirstResponder()
            }
            if textField == txtOtp3 {
                txtOtp4?.becomeFirstResponder()
            }
            if textField == txtOtp4 {
                txtOtp4?.becomeFirstResponder()
                textField.text? = string
                
            }
            textField.text? = string
            return false
        }else{
            if textField == txtOtp1 {
                txtOtp1?.becomeFirstResponder()
            }
            if textField == txtOtp2 {
                txtOtp1?.becomeFirstResponder()
            }
            if textField == txtOtp3 {
                txtOtp2?.becomeFirstResponder()
            }
            if textField == txtOtp4 {
                txtOtp3?.becomeFirstResponder()
            }
            textField.text? = string
            return false
        }
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
}
