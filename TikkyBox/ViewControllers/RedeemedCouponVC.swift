//
//  RedeemedCouponVC.swift
//  TikkyBox
//
//  Created by Sandeep Kumar on 25/06/2020.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SideMenu

class RedeemedCouponVC: UIViewController, UITableViewDataSource,UITableViewDelegate {
    
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var numOfCoupon: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var blankImgView: UIImageView!
    var couponList = [JSON]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getRedeemedCoupon()
    }
    
    @IBAction func tapOnHamburger(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let sidemenuvc = storyboard.instantiateViewController(withIdentifier: "MenuSlideVC") as! MenuSlideVC
        let menu = SideMenuNavigationController(rootViewController: sidemenuvc)
        menu.statusBarEndAlpha = 0
        menu.menuWidth = UIScreen.main.bounds.width / 1.3
        menu.presentationStyle = .menuDissolveIn
        menu.leftSide = true
        menu.presentationStyle.presentingEndAlpha = 0.65
        menu.presentationStyle.onTopShadowOpacity = 0.5
        menu.presentationStyle.onTopShadowRadius = 5
        menu.presentationStyle.onTopShadowColor = .black
        present(menu, animated: true, completion: nil)
    }

    @IBAction func backAction(_ sender: Any) {
           self.navigationController?.popViewController(animated: true)
       }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return couponList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RedeemedCell", for: indexPath) as! RedeemedCell
        
        cell.bgView.layer.cornerRadius = 10
        cell.bgView.layer.shadowColor = UIColor.lightGray.cgColor
        cell.bgView.layer.shadowOpacity = 1
        cell.bgView.layer.shadowOffset = CGSize.zero
        cell.bgView.layer.shadowRadius = 2.5
        
        cell.topView.roundCorners(cornerRadius: 10)
        
        let imagename = "\(GlobalURL.imagebaseURL)\(couponList[indexPath.row]["coupon_image"].stringValue)"
        
        let urlString = imagename.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        let imgURL = URL(string: urlString )
        
        cell.iconImgView?.af_setImage(withURL: imgURL!, placeholderImage:  UIImage(named: "sample_icon3"))
        
        cell.storeLabel.text = couponList[indexPath.row]["store_name"].stringValue
        
        cell.priceLabel.text = "$\( couponList[indexPath.row]["coupon_price"].stringValue)"
        
        cell.dateLabel.text = formattedDateFromString(dateString: couponList[indexPath.row]["created_at"].stringValue, withFormat: "yyyy-MM-dd")
        cell.consumerLabel.text = couponList[indexPath.row]["first_name"].stringValue
        cell.addressLabel.text = couponList[indexPath.row]["store_address"].stringValue
       
        
        
        return cell
    }
    
    func formattedDateFromString(dateString: String, withFormat format: String) -> String? {
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        if let date = inputFormatter.date(from: dateString) {
            let outputFormatter = DateFormatter()
            outputFormatter.dateFormat = format
            return outputFormatter.string(from: date)
        }
        return nil
    }
    
    
    func getRedeemedCoupon(){
        
        
        let baseUrl = "\(GlobalURL.baseURL3)\(GlobalURL.retailRedeemedURL)"
        
        let userId = UserDefaults.standard.getUserId()
        let params = ["auth":"\(userId)"]
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseUrl, method: .post,parameters: params).responseJSON { response in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            if response.result.value != nil{
                let swiftyData = JSON(response.result.value!)
                
                print(swiftyData)
                
                if swiftyData["status"].stringValue == "true"{
                    
                   
                    self.couponList = swiftyData["result"].arrayValue
                    self.tableView.reloadData()
                    
                    self.numOfCoupon.text = "Total Claimed Coupons : \( self.couponList.count)"
                   
                    if self.couponList.count > 0{
                         self.tableView.isHidden = false
                         self.blankImgView.isHidden = true
                        self.topView.isHidden = false
                        
                    }else{
                        self.tableView.isHidden = true
                        self.blankImgView.isHidden = false
                       self.topView.isHidden = true
                        
                    }
                    
                }else{
                    
                    self.tableView.isHidden = true
                     self.blankImgView.isHidden = false
                    self.topView.isHidden = true
                }
            }
        }
        
    }
}
