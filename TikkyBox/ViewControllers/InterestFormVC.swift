//
//  InterestFormVC.swift
//  TikkyBox
//
//  Created by Sandeep Kumar on 01/07/2020.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit

class InterestFormVC: UIViewController {
    
    
    @IBOutlet var views: [UIView]!
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var txtMessage: UITextView!
    
    @IBOutlet weak var saveBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        setUI()
        
    }
    
    func setUI(){
        
        saveBtn.layer.masksToBounds = true
        saveBtn.layer.cornerRadius = 5.0
        
        for item in views{
            
            item.layer.cornerRadius = 10
            item.layer.shadowColor = UIColor.lightGray.cgColor
            item.layer.shadowOpacity = 1
            item.layer.shadowOffset = CGSize.zero
            item.layer.shadowRadius = 2.5
            
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveAction(_ sender: Any) {
    }
}
