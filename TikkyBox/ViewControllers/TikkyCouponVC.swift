//
//  TikkyCouponVC.swift
//  TikkyBox
//
//  Created by Himanshu on 05/03/20.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage

class TikkyCouponVC: UIViewController {

    @IBOutlet weak var tblProduct: UITableView!
    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var emptyImgView: UIImageView!
    var categoryId = String()
    var categoryType = String()
    var couponArray = [JSON]()
    
        
    override func viewDidLoad() {
        super.viewDidLoad()

        tblProduct.dataSource = self
        tblProduct.delegate = self
        tblProduct.separatorStyle = .none
        
        
        
        getCouponList()
        
    }
    

    @IBAction func tapOnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func viewBtnAction(_ sender: UIButton) {
         
         let position = sender.convert(CGPoint.zero, to: tblProduct)
         let indexPath = tblProduct.indexPathForRow(at: position)!
         
         
         
         let storyboard = UIStoryboard(name: "Main", bundle: nil)
               let vc = storyboard.instantiateViewController(identifier: "ProductDetailVC") as! ProductDetailVC
               vc.productArray = couponArray[indexPath.row]
//               vc.isfromWishlist = true
               self.navigationController?.pushViewController(vc, animated: true)
         
     }
     
     @IBAction func likeAction(_ sender: UIButton) {
         let position = sender.convert(CGPoint.zero, to: tblProduct)
         let indexPath = tblProduct.indexPathForRow(at: position)!
         
         
         likeAndDislike(id:  couponArray[indexPath.row]["coupon_id"].stringValue)
     }
    
    
}
extension TikkyCouponVC: UITableViewDataSource , UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return couponArray.count
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CouponTikkyCell", for: indexPath) as! CouponTikkyCell
        cell.selectionStyle = .none
        
        cell.lblCpnTitle.text = couponArray[indexPath.row]["coupon_name"].stringValue
        cell.lblPrice.text = "$\(couponArray[indexPath.row]["coupon_price"].stringValue)"
        cell.lblCutPrice.text = "$\(couponArray[indexPath.row]["cut_price"].stringValue)"
        cell.lblDetail.text = couponArray[indexPath.row]["coupon_descriptions"].stringValue
        cell.lblExpireDate.text = "Expire on: \( couponArray[indexPath.row]["coupon_end_date"].stringValue)"
        cell.lblStoreName.text = couponArray[indexPath.row]["store_name"].stringValue
        let imagename = "\(GlobalURL.imagebaseURL)\(couponArray[indexPath.row]["coupon_image"].stringValue)"
        
        let urlString = imagename.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        
        let imgURL = URL(string: urlString )
        
        cell.imgProduct?.af_setImage(withURL: imgURL!, placeholderImage:  UIImage(named: "sample_icon3"))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
    }
    
    
    
    func getCouponList(){
        
        
        let baseUrl = "\(GlobalURL.baseURL2)\(GlobalURL.couponWishListURL)"
        
        let token = UserDefaults.standard.value(forKey: "token")!
        let params:[String:Any] = ["auth":"\(token)"]
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        Alamofire.request(baseUrl, method: .get,parameters: params).responseJSON { response in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            if response.result.value != nil{
                let swiftyData = JSON(response.result.value!)
                
                print(swiftyData)
                
                if swiftyData["status"].stringValue == "true"{
                    
                    self.couponArray = swiftyData["result"].arrayValue
                    
                    self.tblProduct.isHidden = false
                    self.messageLabel.isHidden = true
                    self.emptyImgView.isHidden = true
                    DispatchQueue.main.async {
                        self.tblProduct.reloadData()
                    }
                   
                }else{
                    self.couponArray.removeAll()
//                    self.createAlert(title: AppName, message: swiftyData["message"].stringValue)
                    
                    self.tblProduct.isHidden = true
                    self.messageLabel.isHidden = false
                    self.emptyImgView.isHidden = false
                    
                    DispatchQueue.main.async {
                        self.tblProduct.reloadData()
                    }
                }
            }
        }
        
    }
    
    func likeAndDislike(id:String){
        /*auth
        coupon_id*/
        
        let baseUrl = "\(GlobalURL.baseURL2)\(GlobalURL.likeDislikeURL)"
        
        let token = UserDefaults.standard.value(forKey: "token")!
        
        let params:[String:Any] = ["auth":"\(token)","coupon_id":"\(id)"]
        
        MBProgressHUD.showHUDMessage(message: "", PPview: self.view)
        
        Alamofire.request(baseUrl, method: .get,parameters: params).responseJSON { response in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            if response.result.value != nil{
                let swiftyData = JSON(response.result.value!)
                
                print(swiftyData)
                
                if swiftyData["status"].stringValue == "true"{
                    let alert = UIAlertController(title: AppName, message: swiftyData["message"].stringValue, preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (UIAlertAction) in
                        self.getCouponList()
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                    
                   
                }else{
                    
                    
                }
            }
        }
        
    }
    
}
