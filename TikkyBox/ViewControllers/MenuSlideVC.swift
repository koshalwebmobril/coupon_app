//
//  MenuSlideVC.swift
//  TikkyBox
//
//  Created by Himanshu on 05/03/20.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit

class MenuSlideVC: UIViewController {
    
    @IBOutlet weak var tblSideMenu: UITableView!
    
    var sideMenuData = ["Home" , "About" , "My Coupons" , "Redeemed Coupon" , "Rewards" , "Report" , "Win Promotion" ,"WishList","Contact Us" , "Log Out"  ]
    
    var manufacturerMenu = ["Home" , "About" ,"Redeem", "Add Coupon","Add Payment","Stores","Products","Win Promotion", "Contact Us" , "Log Out"  ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblSideMenu.delegate = self
        tblSideMenu.dataSource = self
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if UserDefaults.standard.getUserType() == "Retailer"{
            sideMenuData = manufacturerMenu
            
        }else if UserDefaults.standard.getUserType() == "Manufacturer"{
            sideMenuData = manufacturerMenu
        }
    }
    
}

extension MenuSlideVC: UITableViewDataSource , UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sideMenuData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTblCell" , for: indexPath) as! SideMenuTblCell
        cell.titleLabel.text = sideMenuData[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = indexPath.row
        
        if UserDefaults.standard.getUserType() == "Retailer" || UserDefaults.standard.getUserType() == "Manufacturer"{
        
        switch index {
        case 0:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "RedeemedCouponVC") as! RedeemedCouponVC
            self.navigationController?.pushViewController(vc, animated: true)
        case 1:
           let storyboard = UIStoryboard(name: "Main", bundle: nil)
           let vc = storyboard.instantiateViewController(identifier: "AboutusVC") as! AboutusVC
           self.navigationController?.pushViewController(vc, animated: true)
            
            case 2:
                       let storyboard = UIStoryboard(name: "Main", bundle: nil)
                       let vc = storyboard.instantiateViewController(identifier: "RedeemVC") as! RedeemVC
                       self.navigationController?.pushViewController(vc, animated: true)
        
        case 3:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "AddedCouponListVC") as! AddedCouponListVC
            self.navigationController?.pushViewController(vc, animated: true)
//        case 4:
//           let storyboard = UIStoryboard(name: "Main", bundle: nil)
//           let vc = storyboard.instantiateViewController(identifier: "RedeemedCouponVC") as! RedeemedCouponVC
//           self.navigationController?.pushViewController(vc, animated: true)
        case 4:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "PaymentDetailVC") as! PaymentDetailVC
            self.navigationController?.pushViewController(vc, animated: true)
            print("")
            case 5:
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(identifier: "StoreListVC") as! StoreListVC
                self.navigationController?.pushViewController(vc, animated: true)
            case 6:
               let storyboard = UIStoryboard(name: "Main", bundle: nil)
               let vc = storyboard.instantiateViewController(identifier: "ProductListVC") as! ProductListVC
               self.navigationController?.pushViewController(vc, animated: true)
           
        case 7:
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "PromotionVC") as! PromotionVC
        self.navigationController?.pushViewController(vc, animated: true)
        print("")
            case 8:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "ContactusVC") as! ContactusVC
            self.navigationController?.pushViewController(vc, animated: true)
        case 9:
            
            let alert = UIAlertController(title: AppName, message: "Do you want to logout?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: {_ in
                
                UserDefaults.standard.setUserId(userId: "")
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(identifier: "LoginVC") as! LoginVC
                self.navigationController?.pushViewController(vc, animated: true)
            }))
            
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            
        default:
            print("")
        }
        }else {
        
        switch index {
        case 0:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "CustomerHomeVC") as! CustomerHomeVC
            self.navigationController?.pushViewController(vc, animated: true)
        case 1:
           let storyboard = UIStoryboard(name: "Main", bundle: nil)
           let vc = storyboard.instantiateViewController(identifier: "AboutusVC") as! AboutusVC
           self.navigationController?.pushViewController(vc, animated: true)
            
        
        case 2:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "CouponListingVC") as! CouponListingVC
            self.navigationController?.pushViewController(vc, animated: true)
        case 3:
           let storyboard = UIStoryboard(name: "Main", bundle: nil)
           let vc = storyboard.instantiateViewController(identifier: "MyCouponVC") as! MyCouponVC
           self.navigationController?.pushViewController(vc, animated: true)
        case 4:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "RewardsVC") as! RewardsVC
            self.navigationController?.pushViewController(vc, animated: true)
            print("")
        case 5:
           let storyboard = UIStoryboard(name: "Main", bundle: nil)
           let vc = storyboard.instantiateViewController(identifier: "ReportVC") as! ReportVC
           self.navigationController?.pushViewController(vc, animated: true)
        case 6:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "PromotionVC") as! PromotionVC
            self.navigationController?.pushViewController(vc, animated: true)
            print("")
            
            case 7:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "TikkyCouponVC") as! TikkyCouponVC
            self.navigationController?.pushViewController(vc, animated: true)
        
            case 8:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "ContactusVC") as! ContactusVC
            self.navigationController?.pushViewController(vc, animated: true)
        case 9:
            
            let alert = UIAlertController(title: AppName, message: "Do you want to logout?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: {_ in
                
                UserDefaults.standard.setUserId(userId: "")
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(identifier: "LoginVC") as! LoginVC
                self.navigationController?.pushViewController(vc, animated: true)
            }))
            
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            
        default:
            print("")
        }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    
    
}





class SideMenuTblCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    
}
