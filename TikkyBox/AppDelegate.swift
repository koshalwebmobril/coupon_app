//
//  AppDelegate.swift
//  TikkyBox
//
//  Created by Himanshu on 04/03/20.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
         IQKeyboardManager.shared.enable = true
        GIDSignIn.sharedInstance().clientID = "382640231486-0n7f24rnvh41ea65j34p7usbqp52jiro.apps.googleusercontent.com"
       Thread.sleep(forTimeInterval: 2)
//         ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
//         let data = UserDefaults.standard.value(forKey: "token") as? String
        
        if UserDefaults.standard.getUserId() == "" {
            gotoLoginVC()
            
            
        }else{
            if UserDefaults.standard.getUserType() == "Customer"{
                gotoHomeVC()
            }else{
                gotoRetailerHomeVC()
            }
             
            
        }
        
     
        return true
      
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        
    }

    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
    }
    
    
    func applicationWillTerminate(_ application: UIApplication) {
    
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
    }
    
    
    
    @available(iOS 9.0, *)
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
      return GIDSignIn.sharedInstance().handle(url)
    }
    
    func gotoLoginVC(){
        
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        let navigationController = UINavigationController(rootViewController: initialViewController)
        navigationController.isNavigationBarHidden = true
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
    }
    
    func gotoHomeVC(){
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController = mainStoryBoard.instantiateViewController(withIdentifier: "SelectCategoryVC") as! SelectCategoryVC
       
        let navigationController = UINavigationController(rootViewController: initialViewController)
        navigationController.isNavigationBarHidden = true
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
    }
    
    func gotoRetailerHomeVC(){
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController = mainStoryBoard.instantiateViewController(withIdentifier: "RedeemedCouponVC") as! RedeemedCouponVC
       
        let navigationController = UINavigationController(rootViewController: initialViewController)
        navigationController.isNavigationBarHidden = true
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
    }


}

