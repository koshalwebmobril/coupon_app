//
//  AlertHandler.swift
//  NexReno
//
//  Created by RipenApps on 04/10/18.
//  Copyright © 2018 RipenApps. All rights reserved.
//

import UIKit

class AlertHandler: NSObject {
    static func makeAlertWithSingleAction(withTitle title:String, withMessage message:String, withButtonTitle buttonTitle:String, withHandler handler:@escaping () -> Void) -> UIAlertController{
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let firstAction = UIAlertAction(title: buttonTitle, style: .default, handler: {
            action in
            handler()
        })
        alert.addAction(firstAction)
        return alert
    }

}
