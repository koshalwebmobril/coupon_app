//
//  Extension.swift
//  Dayshare
//
//  Created by Apple on 25/02/19.
//  Copyright © 2019 Ripenapps. All rights reserved.
//

import Foundation
import UIKit
//import AKSideMenu
import Alamofire

let appDelegate  = UIApplication.shared.delegate! as! AppDelegate

extension UIViewController {
    
    
    func createAlert (title:String, message:String)
    {
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in})
        alertView.addAction(action)
        self.present(alertView, animated: true, completion: nil)
    }
    
    
}


extension UIView
{
    func dropShadow(shadowColor: UIColor = UIColor.lightGray, borderColor: UIColor = UIColor.white , opacity: Float = 0.5, offSet: CGSize = CGSize(width: 0, height: 0), radius: CGFloat = 5, scale: Bool = true) {
        
        self.layer.masksToBounds = false
        self.layer.borderColor = borderColor.cgColor
        self.layer.shadowColor = shadowColor.cgColor
        self.layer.shadowOpacity = opacity
        self.layer.shadowOffset = offSet
        self.layer.shadowRadius = radius
        //    self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    func makeRounded() {
        self.layer.cornerRadius = self.frame.size.width/2
        self.clipsToBounds = true
    }
    
    func makeRoundCorner(_ radius:CGFloat) {
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }
    
    func makeBorder(_ radius:CGFloat)
    {
        self.layer.borderWidth = radius
        self.layer.borderColor = UIColor(red: 199, green: 72, blue: 8).cgColor
    }
    
    func makeRoundCornerSpecific(corners: UIRectCorner, radius: CGFloat) {
           let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
           let mask = CAShapeLayer()
           mask.path = path.cgPath
           layer.mask = mask
       }
    
}

class CommonUtilities
{
    
    static let shared = CommonUtilities()
    
    
    func isValidEmail(testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
   
    
    func getFormattedDates(string: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd" // This formate is input formated .
        
        if let _ = dateFormatter.date(from: "\(string)") {
            //date parsing succeeded, if you need to do additional logic, replace _ with some variable name i.e date
            let formateDate = dateFormatter.date(from:"\(string)")!
            dateFormatter.dateFormat = "MMM dd,yyyy"
            return dateFormatter.string(from: formateDate)
        } else {
            // Invalid date
            return string
        }
        
    }
    
    
    class func getFormattedDate(string: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd" // This formate is input formated .
        let formateDate = dateFormatter.date(from:"\(string)")!
        dateFormatter.dateFormat = "MMM dd,yyyy" // Output Formated
        return dateFormatter.string(from: formateDate)
        print ("Print :\(dateFormatter.string(from: formateDate))")//Print :02-02-2018
    }
 
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
}


extension UIColor {
   convenience init(red: Int, green: Int, blue: Int) {
       assert(red >= 0 && red <= 255, "Invalid red component")
       assert(green >= 0 && green <= 255, "Invalid green component")
       assert(blue >= 0 && blue <= 255, "Invalid blue component")

       self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
   }

   convenience init(rgb: Int) {
       self.init(
           red: (rgb >> 16) & 0xFF,
           green: (rgb >> 8) & 0xFF,
           blue: rgb & 0xFF
       )
   }
}
extension UIStackView {
    func addBackground(color: UIColor) {
        let subView = UIView(frame: bounds)
        subView.backgroundColor = color
        subView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        insertSubview(subView, at: 0)
    }
}


extension Date {
   func toString() -> String {
      let dfToString = DateFormatter()
      dfToString.dateFormat = "dd MMM yyyy"
      return dfToString.string(from: self)
   }
}
extension String {
   func toDate() -> Date? {
      let dfToDate = DateFormatter()
      dfToDate.dateFormat = "yyyy-MM-dd’T’HH:mm:ssZZZZZ"
      return dfToDate.date(from: self)
   }
}
