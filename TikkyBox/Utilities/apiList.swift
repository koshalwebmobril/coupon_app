//
//  apiList.swift
//  Ziphawk_Child
//
//  Created by Sushant on 17/07/19.
//  Copyright © 2019 WebMobril. All rights reserved.
//

import Foundation

struct GlobalURL {

    static let baseURL = "http://webmobril.org/dev/tikkybox/api/auth/"
   
    static let imagebaseURL = "http://webmobril.org/dev/tikkybox/"
    
    static let loginURL = "login"
    static let registerURL = "register"
    static let verifyOtpURL = "verifyotp"
    static let resendOtpURL = "resendotp"
    static let forgotPasswordURL = "forgotpassword"
    static let logoutURL = "logout"
    static let socialLoginURL = "socialLogin"
    
    static let baseURL2 = "https://webmobril.org/dev/tikkybox/api/product/"
    
    static let categoryURL = "categories"
    static let winPromotionURL = "winpromotions"
    static let winPromotionDetailURL = "winpromotions_detail"
    static let homeURL = "home"
    static let moreCouponURL = "more_coupons"
    static let couponDetailURL = "coupons_detail"
    static let likeDislikeURL = "like_dislike_coupon"
    static let couponWishListURL = "coupon_wishlist"
    static let claimedRewardsURL = "rewards"
    static let rewardsURL = "rewards_list"
     static let claimRewardsURL = "claim_reward"
    static let addCouponURL = "coupon"
    static let getCouponURL = "coupon"
    static let deleteCouponURL = "coupon_delete"
    static let addPaymentDetailURL = "payment"
    static let updatePaymentDetailURL = "update_payment"
    
    static let baseURL3 = "https://webmobril.org/dev/tikkybox/api/"
    
    static let storeURL = "store"
    static let updateStoreURL = "store/update_store"
    static let deleteStoreURL = "store/store_delete"
    static let productListURL = "store/product"
    static let addProductURL = "store/product"
    static let deleteProductURL = "store/product_delete"
    static let updateProductURL = "store/update_product"
    static let custRedeemURL = "request-redeem"
    static let couponListURL = "coupons"
    static let retailRedeemedURL = "retailer-redeemed"
    static let custRedeemedURL = "customer-redeemed"
    static let shopRedeemURL = "redeem"
    static let custHomeURL = "stats"
    
    static let addWinPromotionURL = "promotions/create"
    static let listWinPromotionURL = "promotions/list"
   
    
   
    
    
   
    
}
