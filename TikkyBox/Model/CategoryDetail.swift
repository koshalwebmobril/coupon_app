//
//  CategoryDetail.swift
//  TikkyBox
//
//  Created by Himanshu on 16/03/20.
//  Copyright © 2020 Himanshu. All rights reserved.
//

import Foundation

struct CategoryData: Codable {
    let result: Result
}

// MARK: - Result
struct Result: Codable {
    let status: Bool
    let categories: [Category]
}

// MARK: - Category
struct Category: Codable {
    let id, name: String
    let image: String?
}
