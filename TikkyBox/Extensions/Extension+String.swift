//
//  Extension+String.swift
//  Plabook
//
//  Created by Mac on 11/15/19.
//  Copyright © 2019 mobappssolutions.com. All rights reserved.
//

import Foundation

let REGEX_USER_NAME_LIMIT="^.{3,15}$"
let REGEX_USER_NAME="[A-Za-z0-9]{1,50}"
let REGEX_USER_EMAIL="[A-Z0-9a-z._%+-]{2,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$"
let REGEX_USER_PASSWORD_LIMIT="^.{6,20}$"
let REGEX_USER_PASSWORD="^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{6,}$"
let REGEX_USER_PHONE_PATTERN="[0-9]{3}\\-[0-9]{3}\\-[0-9]{4}"
let REGEX_USER_PHONE="^[0-9]{7,15}$"
let REGEX_ONLY_NUMBER="[0-9]{1,10}"
let REGEX_RANGE_PATTERN="[0-9]{1,3}\\-[0-9]{1,3}"
let REGEX_ZIPCODE="^[0-9]{3,6}$"
let REGEX_ALPHNUM="^[A-Z]{2,5}[A-Z0-9]{6,9}$"



extension String {
    
    func isValidURL() -> Bool {
           guard !contains("..") else { return false }

           let head     = "((http|https)://)?([(w|W)]{3}+\\.)?"
           let tail     = "\\.+[A-Za-z]{2,3}+(\\.)?+(/(.)*)?"
           let urlRegEx = head+"+(.)+"+tail

           let urlTest = NSPredicate(format:"SELF MATCHES %@", urlRegEx)
           return urlTest.evaluate(with: trimmingCharacters(in: .whitespaces))
       }
    
    func isValidEmail() -> Bool {
        
        let regex = try! NSRegularExpression(pattern: REGEX_USER_EMAIL,options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
    
    func isValidALPHANUMBER() -> Bool {
        
        let regex = try! NSRegularExpression(pattern: REGEX_ALPHNUM,options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }

    
    func isValidPassword() -> Bool {
        
        let regex = try! NSRegularExpression(pattern: REGEX_USER_PASSWORD,options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
    
    func isValidPhoneNumber() -> Bool {
        
        let regex = try! NSRegularExpression(pattern: REGEX_USER_PHONE,options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
    func isValidTextField() -> Bool {
        
        let regex = try! NSRegularExpression(pattern: REGEX_USER_NAME,options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
    
    func isValidZipcode() -> Bool {
        
        let regex = try! NSRegularExpression(pattern: REGEX_ZIPCODE,options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
}


extension Date {
    static func getFormattedDate(date: Date, format: String) -> String {
        let dateformat = DateFormatter()
        dateformat.dateFormat = format
        return dateformat.string(from: date)
    }
}


extension UIColor{
    public class var baseGray: UIColor {
        return UIColor(red: 242/255, green: 242/255, blue: 242/255, alpha: 1.0)
    }
    public class var baseRed: UIColor {
        return UIColor(red: 255/255, green: 35/255, blue: 108/255, alpha: 1.0)
    }
}



extension UserDefaults{
    
    func setPhone(token:String)  {
        
        self.set(token, forKey: "phone")
        
    }
    
    func getPhone() -> String {
        return self.value(forKey: "phone") as? String ?? "92929229292"
    }
    
    func setDeviceToken(token:String)  {
        
        self.set(token, forKey: "device_token")
        
    }
    
    func getDeviceToken() -> String {
        return self.value(forKey: "device_token") as? String ?? "12345"
    }
    
    func setUserId(userId:String)  {
        
        self.set(userId, forKey: "token")
        
    }
    
    func getUserId() -> String {
        
        
        return self.value(forKey: "token") as? String ?? ""
    }
    
    func setLoginFlag(loginFlag:Int)  {
        
        self.set(loginFlag, forKey: "login_flag")
        
    }
    
    func getLoginFlag() -> Int {
        return self.value(forKey: "login_flag") as! Int
    }
    
    func removeUserdefaultData()  {
        
        self.removeObject(forKey: "user_id")
        self.removeObject(forKey: "login_flag")
        
    }
    
    func setCategoryId(categoryId:String)  {
        
        self.set(categoryId, forKey: "categoryId")
        
    }
    
    func getCategoryId() -> String {
        
        
        return self.value(forKey: "categoryId") as? String ?? ""
    }
    
    
    func setUserType(type:String)  {
        
        self.set(type, forKey: "type")
        
    }
    
    func getUserType() -> String {
        return self.value(forKey: "type") as? String ?? ""
    }
    
    
    
}


extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}



extension UIView {
  
    func roundCorners(cornerRadius: Double) {
        self.layer.cornerRadius = CGFloat(cornerRadius)
        self.clipsToBounds = true
        self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }
   
}

extension UITextField{
    func setRightView(image:String,frame:CGRect){
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: self.frame.height))
        
        let button = UIButton(frame: frame)
        button.setImage(UIImage(named: image), for: .normal)
        let imageView = UIImageView(frame: frame)
               imageView.contentMode = .scaleAspectFit
               imageView.image = UIImage(named: image)
        view.addSubview(button)
               
        self.rightViewMode = .always
        self.rightView = view
        
    }
}


extension UIButton{
    
    func underline() {
        guard let text = self.titleLabel?.text else { return }
        let attributedString = NSMutableAttributedString(string: text)
        //NSAttributedStringKey.foregroundColor : UIColor.blue
        attributedString.addAttribute(NSAttributedString.Key.underlineColor, value: self.titleColor(for: .normal)!, range: NSRange(location: 0, length: text.count))
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: self.titleColor(for: .normal)!, range: NSRange(location: 0, length: text.count))
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: text.count))
        self.setAttributedTitle(attributedString, for: .normal)
    }

}
